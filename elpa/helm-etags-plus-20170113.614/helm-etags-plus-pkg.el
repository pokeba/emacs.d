;;; -*- no-byte-compile: t -*-
(define-package "helm-etags-plus" "20170113.614" "Another Etags helm.el interface" '((helm "1.7.8")) :commit "704f0991ee4a2298b01c33aafc224eef322e15e3" :keywords '("helm" "etags") :url "https://github.com/jixiuf/helm-etags-plus")
