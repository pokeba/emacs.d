;;; -*- no-byte-compile: t -*-
(define-package "counsel-etags" "20180324.2005" "Fast and complete Ctags/Etags solution using ivy" '((emacs "24.4") (counsel "0.9.1")) :commit "267c071724fcc665359f2f4472392507facaab2c" :keywords '("tools" "convenience") :url "http://github.com/redguardtoo/counsel-etags")
