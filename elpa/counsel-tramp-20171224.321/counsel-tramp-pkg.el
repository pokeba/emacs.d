;;; -*- no-byte-compile: t -*-
(define-package "counsel-tramp" "20171224.321" "Tramp ivy interface for ssh, docker, vagrant" '((emacs "24.3") (counsel "0.10")) :commit "6efa0e6e204d08d5b8b8b66f7e3ae7f07d5a3665" :url "https://github.com/masasam/emacs-counsel-tramp")
