;;; -*- no-byte-compile: t -*-
(define-package "counsel-etags" "20180304.58" "Fast and complete Ctags/Etags solution using ivy" '((emacs "24.4") (counsel "0.9.1")) :commit "e97902f9947c663372698affd1577a8d23876561" :url "http://github.com/redguardtoo/counsel-etags" :keywords '("tools" "convenience"))
