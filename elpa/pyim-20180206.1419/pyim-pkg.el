(define-package "pyim" "20180206.1419" "A Chinese input method support quanpin, shuangpin, wubi and cangjie."
  '((emacs "24.4")
    (popup "0.1")
    (async "1.6")
    (pyim-basedict "0.1"))
  :url "https://github.com/tumashu/pyim" :keywords
  '("convenience" "chinese" "pinyin" "input-method"))
;; Local Variables:
;; no-byte-compile: t
;; End:
