;;;;
;;;; ido mode
;;;;
(require 'ido)

;;;;(setq ido-save-directory-list-file (cjp-emacs-structure-dir ".ido.last")) 
(ido-mode t) 
(setq ido-everywhere t 
      ido-enable-flex-matching t 
      ido-create-new-buffer 'always) ; If a buffer name that doesn't exist is 
                                        ; chosen, just make a new one without 
                                        ; prompting

;;; Order extensions by how I use them 
(setq ido-file-extensions-order '(".org"  ".txt" ".cpp" ".hpp" ".c" ".h" ".py" ".sh" ".el" ".xml" ".htm")) 

;;; Ignore files defined in variable completion-ignored-extensions 
(setq ido-ignore-extensions t) 

;;; this allow 2 emacsclients show the same file.
(setq ido-default-buffer-method 'selected-window)
