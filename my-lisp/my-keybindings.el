;;;;Add ratpoison-like buffer movement.  First two require
;;;;cyclebuffer.el (available in the emacs-goodies-el Debian package).
;;(global-set-key "\356" (quote cycle-buffer-forward))    ;;M-n
;;(global-set-key "\360" (quote cycle-buffer-backward))   ;;M-p
;;(global-set-key "\215" (quote mode-line-other-buffer)) ;;M-RET
(global-set-key (quote [C-tab]) (quote other-window)) ;;C-Tab

;;;;
;; Found out that emacs by default will automatically set 
;;     [f3] to 'start-kbd-macro 
;;     [f4] to if a keyboard macro is being defined, end the definition; 
;;          otherwise, execute the most recent keyboard macro 
;;          (kmacro-end-or-call-macro). 
;; So just use the default keys [f3] and [f4] without redefining them.
;;;;
;;; Move origianl [f3], [f4] to [f11], [12] as they caused me too many problems
;;; due to missed typing on keys.

(global-set-key (quote [f2]) 'xah-copy-line-or-region) ;; copy.
(global-set-key (quote [S-f2]) 'xah-cut-line-or-region) ;; cut

(global-set-key (quote [f3]) 'yank) ;; paste
;;(global-set-key (quote [S-f3]) (quote xah-show-kill-ring)) ;; show kill-ring

;; show matching parenthesis witgh matching parenthesis only
(global-set-key (quote [f4]) 'enable-paren-expression)
;; show matching parenthesis with expression
(global-set-key (quote [S-f4]) 'disable-paren-expression)

(global-set-key (quote [f5]) 'undo) ;; undo
(global-set-key [S-f5] 'xah-extend-selection)

;;(global-set-key (quote [S-f6]) (quote compare-windows)) ;; compare text in 2 windows; 

(global-set-key [f6] 'named-term)
(global-set-key [S-f6] 'named-eshell)

(global-set-key [f7] 'ediff-buffers)
(global-set-key [S-f7] 'named-shell)

(global-set-key (quote [f8]) 'delete-current-line) ;; kill-line; 

(global-set-key (quote [f9]) 'revert-buffer)

(global-set-key (quote [f10]) 'man) ;; compare text in 2 windows; 
(global-set-key [S-f10] 'help)

(global-set-key [f11] 'move-line-up)
(global-set-key [S-f11] 'move-line-down)

(global-set-key [f12] (quote kmacro-end-or-call-macro))
(global-set-key [S-f12] 'start-kbd-macro)


;;;; redefine function keys to non-function keys
(global-set-key (kbd "C-x a 2") (quote xah-copy-line-or-region)) ;; copy.
(global-set-key (kbd "C-x w 2") (quote xah-cut-line-or-region)) ;; cut

(global-set-key (kbd "C-x a 3") (quote yank)) ;; paste
(global-set-key (kbd "C-x w 3") (quote xah-show-kill-ring)) ;; show kill-ring

;; show matching parenthesis witgh matching parenthesis only
(global-set-key (kbd "C-x a 4") 'enable-paren-expression)
;; show matching parenthesis with expression
(global-set-key (kbd "C-x w 4") 'disable-paren-expression)

(global-set-key (kbd "C-x a 5") 'undo) ;; undo
(global-set-key (kbd "C-x w 5") 'xah-extend-selection)

(global-set-key (kbd "C-x a 6") 'named-term)
(global-set-key (kbd "C-x w 6") 'named-eshell)

(global-set-key (kbd "C-x a 7") (quote ediff-buffers))
(global-set-key (kbd "C-x w 7") 'named-shell)

(global-set-key (kbd "C-x a 8") 'delete-current-line) ;; kill-line; 

(global-set-key (kbd "C-x a 9") (quote revert-buffer))

(global-set-key (kbd "C-x a 0") 'move-line-up)
(global-set-key (kbd "C-x w 0") 'move-line-down)

(global-set-key "\M-s" 'isearch-forward-regexp)

;;;; easy keys to split window. Key based on ErgoEmacs keybinding
;;;; (global-set-key "\M-0" 'delete-window) ;; expand current pane
;;;; (global-set-key "\M-1" 'delete-other-windows) ;; expand current pane
;;;; (global-set-key "\M-2" 'split-window-below) ;; split pane top/bottom
;;;; (global-set-key "\M-5" 'split-window-right) ;; split window left/right
;;;; (global-set-key "\M-3" 'other-window) ;; cursor to other pane
(global-set-key (kbd "C-x 0") 'delete-window) ;; expand current pane
(global-set-key (kbd "C-x 1") 'delete-other-windows) ;; expand current pane
(global-set-key (kbd "C-x 2") 'split-window-below) ;; split pane top/bottom
(global-set-key (kbd "C-x 5") 'split-window-right) ;; split window left/right
(global-set-key (kbd "C-x o") 'other-window) ;; cursor to other pane

;;;; `C-x ^’ makes the current window taller (‘enlarge-window’)
;;;; `C-x }’ makes it wider (‘enlarge-window-horizontally’)
;;;; `C-x {’ makes it narrower (‘shrink-window-horizontally’)
;;;; The default keybindings might not be appropriate for you,
;;;; however. Here is a very simple suggestion to make them more accessible.
;;;;
;;;; (global-set-key (kbd "S-C-<left>") 'shrink-window-horizontally)
;;;; (global-set-key (kbd "S-C-<right>") 'enlarge-window-horizontally)
;;;; (global-set-key (kbd "S-C-<down>") 'shrink-window)
;;;; (global-set-key (kbd "S-C-<up>") 'enlarge-window)
(global-set-key (kbd "C-x <left>") 'shrink-window-horizontally)
(global-set-key (kbd "C-x <right>") 'enlarge-window-horizontally)
(global-set-key (kbd "C-x <down>") 'shrink-window)
(global-set-key (kbd "C-x <up>") 'enlarge-window)
