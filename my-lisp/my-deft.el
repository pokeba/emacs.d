(require 'deft)
(setq deft-default-extension "txt")
(setq deft-extensions '("txt" "org" "tex"))

;;(setq deft-directory "~/.deft")
(setq deft-recursive t)
;;; deft-text-mode seems obsolete.
;;; I set it with auto-mode-alist instead.
;;;
;;; (setq deft-text-mode 'org-mode)
(setq deft-auto-save-interval 0)
;;key to launch deft
;;(global-set-key (kbd "C-c d") 'deft)

;;;; if set true, it will use first line in each file as title.
;;;; otherwise, it uses filenames as title.
;;;(setq deft-use-filename-as-title t)
(setq deft-use-filename-as-title nil)
(setq deft-use-filter-string-for-filename t)

;;; sets the desired directory to run deft, kills current deft
;;; buffer (which may be in the wrong directory) and then starts deft.
;;; bjm is original author's short name.
(defun bjm-deft (dir)
  "Run deft in directory DIR"
  (setq deft-directory dir)
  (switch-to-buffer "*Deft*")
  (kill-this-buffer)
  (deft)
)

(defun named-deft (dir)
  "Run deft in the specified directory"
  (interactive "deft directory: ")
  (pop-to-buffer dir)
  (setq deft-directory dir)
  (switch-to-buffer "*Deft*")
  (kill-this-buffer)
  (deft)
)

;;
;;advise deft-new-file-named to replace spaces in file names with -
;;
(defun bjm-deft-strip-spaces (args)
  "Replace spaces with - in the string contained in the first element of the list args. Used to advise deft's file naming function."
  (list (replace-regexp-in-string " " "-" (car args)))
  )
(advice-add 'deft-new-file-named :filter-args #'bjm-deft-strip-spaces)

