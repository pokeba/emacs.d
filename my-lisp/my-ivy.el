;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;
;;;; My setup for ivy, counsel
;;;;

(require 'counsel-etags)

(ivy-mode 1)

(setq ivy-use-virtual-buffers t)
(setq ivy-count-format "(%d/%d) ")
(setq enable-recursive-minibuffers t)

;;;; whether to wrap. That is C-n and C-p will cycle through choices.
;;(setq ivy-wrap t)

;;;; The default matcher will use a .* regex wild card
;;;; in place of each single space in the input.
(setq ivy-re-builders-alist
      '((t . ivy--regex-plus)))
;;;; If you want to use the fuzzy matcher,
;;;; which instead uses a .* regex wild card between each input letter
;;(setq ivy-re-builders-alist
;;      '((t . ivy--regex-fuzzy)))


;;;; for colunsel-etags
;;;;

;;;; ignore directories and files
(eval-after-load 'counsel-etags
  '(progn
     ;; counsel-etags-ignore-directories does NOT support wildcast
     (add-to-list 'counsel-etags-ignore-directories "src")
     ;; counsel-etags-ignore-filenames supports wildcast
     (add-to-list 'counsel-etags-ignore-filenames "TAGS")
     (add-to-list 'counsel-etags-ignore-filenames "G*")
     (add-to-list 'counsel-etags-ignore-filenames "*.yang")))
;;;;
;;;; Auto update tags file
;; Don't ask before rereading the TAGS files if they have changed
(setq tags-revert-without-query t)
;; Don't warn when TAGS files are large
(setq large-file-warning-threshold nil)
;; Setup auto update now
(add-hook 'prog-mode-hook
  (lambda ()
    (add-hook 'after-save-hook
              'counsel-etags-virtual-update-tags 'append 'local)))
;;
(add-hook 'after-save-hook 'counsel-etags-virtual-update-tags)

(global-set-key "\C-s" 'swiper)
;;; (global-set-key (kbd "<f6>") 'ivy-resume)
(global-set-key (kbd "<f4>") 'ivy-resume)
(global-set-key (kbd "M-x") 'counsel-M-x)
(global-set-key (kbd "C-x C-f") 'counsel-find-file)
(global-set-key (kbd "<f1> f") 'counsel-describe-function)
(global-set-key (kbd "<f1> v") 'counsel-describe-variable)
(global-set-key (kbd "<f1> l") 'counsel-find-library)
(global-set-key (kbd "<f1> i") 'counsel-info-lookup-symbol)
(global-set-key (kbd "<f1> u") 'counsel-unicode-char)
(global-set-key (kbd "C-c C-r") 'ivy-resume)
;;(global-set-key (kbd "C-c g") 'counsel-git)
;;(global-set-key (kbd "C-c j") 'counsel-git-grep)
(global-set-key (kbd "C-c k") 'counsel-ag)
(global-set-key (kbd "C-x l") 'counsel-locate)
(global-set-key (kbd "C-S-o") 'counsel-rhythmbox)
(define-key minibuffer-local-map (kbd "C-r") 'counsel-minibuffer-history)

;;set action options during execution of counsel-find-file
;; replace "frame" with window to open in new window
(ivy-set-actions
 'counsel-find-file
 '(("j" find-file-other-frame "other frame")
   ("f" find-file-other-window "other window")
   ("b" counsel-find-file-cd-bookmark-action "cd bookmark")
   ("x" counsel-find-file-extern "open externally")
   ("d" delete-file "delete")
   ("r" counsel-find-file-as-root "open as root")))
 
;; set actions when running C-x b
;; replace "frame" with window to open in new window
(ivy-set-actions
 'ivy-switch-buffer
 '(("j" switch-to-buffer-other-frame "other frame")
   ("k" kill-buffer "kill")
   ("r" ivy--rename-buffer-action "rename")))

(defun ivy-yank-action (x)
  (kill-new x))

(defun ivy-copy-to-buffer-action (x)
  (with-ivy-window
    (insert x)))

;;;; This applies the action to "EVERY" command
;;;; Then in any completion session,
;;;; M-o y invokes ivy-yank-action,
;;;; and M-o i invokes ivy-copy-to-buffer-action.
(ivy-set-actions
 t
 '(("i" ivy-copy-to-buffer-action "insert")
   ("y" ivy-yank-action "yank")))

;;;; This reset no action to "EVERY" command
;;(ivy-set-actions t nil)

(ivy-set-actions
 'swiper
 '(("i" ivy-copy-to-buffer-action "insert")
   ("y" ivy-yank-action "yank")))

(add-hook 'c-mode-hook 'counsel-gtags-mode)
(add-hook 'c++-mode-hook 'counsel-gtags-mode)

(with-eval-after-load 'counsel-gtags
  (define-key counsel-gtags-mode-map (kbd "M-t") 'counsel-gtags-find-definition)
;;  (define-key counsel-gtags-mode-map (kbd "M-.") 'counsel-gtags-find-definition)
;;  (define-key counsel-gtags-mode-map (kbd "M-,") 'counsel-gtags-go-backward)
  (define-key counsel-gtags-mode-map (kbd "M-r") 'counsel-gtags-find-reference)
  (define-key counsel-gtags-mode-map (kbd "M-s") 'counsel-gtags-find-symbol))

;;;; above did not work for me.
;;;; So just set key globally
(global-set-key (kbd "M-t") 'counsel-gtags-find-definition)
(global-set-key (kbd "M-r") 'counsel-gtags-find-reference)
(global-set-key (kbd "M-s") 'counsel-gtags-find-symbol)
;; (global-set-key (kbd "M-.") 'counsel-gtags-find-definition)
;; (global-set-key (kbd "M-,") 'counsel-gtags-go-backward)

;;;; counsel-tramp
(define-key global-map (kbd "C-c s") 'counsel-tramp)
