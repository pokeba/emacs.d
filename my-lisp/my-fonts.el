;;(setq bdf-directory-list
;;      '("C:/Intlfonts/Asian"
;;        "C:/Intlfonts/Chinese" "C:/Intlfonts/Chinese.X"
;;        "C:/Intlfonts/Chinese.BIG" "C:/Intlfonts/Ethiopic"
;;        "C:/Intlfonts/European" "C:/Intlfonts/European.BIG"
;;        "C:/Intlfonts/Japanese" "C:/Intlfonts/Japanese.X"
;;        "C:/Intlfonts/Japanese.BIG" "C:/Intlfonts/Korean.X"
;;        "C:/Intlfonts/Misc"))

(setq bdf-directory-list
      '("/mnt/e/data/emacs/fonts/intlfonts/Asian"
        "/mnt/e/data/emacs/fonts/intlfonts/Chinese"
        "/mnt/e/data/emacs/fonts/intlfonts/Chinese.BIG"
        "/mnt/e/data/emacs/fonts/intlfonts/Chinese.X"
        "/mnt/e/data/emacs/fonts/intlfonts/Ethiopic"
        "/mnt/e/data/emacs/fonts/intlfonts/European"
        "/mnt/e/data/emacs/fonts/intlfonts/European.BIG"
        "/mnt/e/data/emacs/fonts/intlfonts/Japanese"
        "/mnt/e/data/emacs/fonts/intlfonts/Japanese.BIG"
        "/mnt/e/data/emacs/fonts/intlfonts/Japanese.X"
        "/mnt/e/data/emacs/fonts/intlfonts/Korean.X"
        "/mnt/e/data/emacs/fonts/intlfonts/Misc"))

;;(setq w32-bdf-filename-alist
;;      (w32-find-bdf-fonts bdf-directory-list))

(create-fontset-from-fontset-spec
 "-*-fixed-medium-r-normal-*-16-*-*-*-c-*-fontset-bdf,
 japanese-jisx0208:-*-*-medium-r-normal-*-16-*-*-*-c-*-jisx0208.1983-*,
 katakana-jisx0201:-*-*-medium-r-normal-*-16-*-*-*-c-*-jisx0201*-*,
 latin-jisx0201:-*-*-medium-r-normal-*-16-*-*-*-c-*-jisx0201*-*,
 japanese-jisx0208-1978:-*-*-medium-r-normal-*-16-*-*-*-c-*-jisx0208.1978-*,
 thai-tis620:-misc-fixed-medium-r-normal--16-160-72-72-m-80-tis620.2529-1,
 lao:-misc-fixed-medium-r-normal--16-160-72-72-m-80-MuleLao-1,
 tibetan-1-column:-TibMdXA-fixed-medium-r-normal--16-160-72-72-m-80-MuleTibetan-1,
 ethiopic:-Admas-Ethiomx16f-Medium-R-Normal--16-150-100-100-M-160-Ethiopic-Unicode,
 tibetan:-TibMdXA-fixed-medium-r-normal--16-160-72-72-m-160-MuleTibetan-0")

(setq font-encoding-alist
      (append '(("MuleTibetan-0" (tibetan . 0))
                ("GB2312"        (chinese-gb2312 . 0))
                ("JISX0208"      (japanese-jisx0208 . 0))
                ("JISX0212"      (japanese-jisx0212 . 0))
                ("VISCII"        (vietnamese-viscii-lower . 0))
                ("KSC5601"       (korean-ksc5601 . 0))
                ("MuleArabic-0"  (arabic-digit . 0))
                ("MuleArabic-1"  (arabic-1-column . 0))
                ("MuleArabic-2"  (arabic-2-column . 0)))
              font-encoding-alist))

(set-default-font "fontset-bdf")
