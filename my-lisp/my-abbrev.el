;; -*- coding: utf-8; lexical-binding: t; -*-
;; sample use of abbrev
;;;
;;; [ from http://ergoemacs.org/emacs/emacs_abbrev_mode.html ]
;;;

(clear-abbrev-table global-abbrev-table)

(define-abbrev-table 'global-abbrev-table
  '(

    ;; name
    ("jo" "Joseph" )
    ("sud" "Sadhu" )
    ("nv" "Neville" )
    ("br" "Bruce" )

    ;; bible study
    ("ab" "[AB]" )
    ("kj" "[KJV]" )
    ("bl" "bible" )
    ("chp" "chapter" )
    ("gd" "God" )
    ("vs" "verse" )
     
    ;; net abbrev
    ("afaik" "as far as i know" )
    ("atm" "at the moment" )
    ("ty" "thank you" )
    ("ui" "user interface" )
    ("ur" "you are" )

    ("btw" "by the way" )

    ("cnt" "can't" )
    ("eg" "e.g." )
    ("ie" "i.e." )

    ;; english word abbrev
    ("ann" "announcement" )
    ("arg" "argument" )
    ("autom" "automatic" )
    ("bc" "because" )
    ("bg" "background" )
    ("bt" "between" )
    ("comm" "communication" )
    ("comp" "computer" )
    ("desc" "description" )
    ("dia" "diabetes" )
    ("dict" "dictionary" )
    ("dir" "directory" )
    ("dirs" "directories" )
    ("disc" "discussion" )
    ("dl" "download" )
    ("eng" "English" )
    ("env" "environment" )
    ("esp" "especially" )
    ("ex" "example" )
    ("fex" "for example," )
    ("fu" "function" )
    ("gvn" "government" )
    ("hex" "hexadecimal" )
    ("ia" "interactive" )
    ("impl" "implementation" )
    ("implt" "implement" )
    ("intn" "international" )
    ("intro" "introduction" )
    ("jp" "Japanese" )
    ("kb" "keyboard" )
    ("kbd" "keybinding" )
    ("kbs" "keyboards" )
    ("kw" "keyword" )
    ("paren" "parenthesis" )
    ("pls" "please" )
    ("prof" "professor" )
    ("ref" "reference" )
    ("techn" "technology" )
    ("trad" "traditional" )
    ("ver" "version" )
    ("vid" "video" )
    ("wp" "Wikipedia" )
    ("alt" "alternative" )
    ("auto" "automatic" )
    ("c" "character" )
    ("chars" "characters" )
    ("dev" "development" )
    ("ergo" "ergonomic" )
    ("exp" "experience" )
    ("expr" "expression" )
    ("l" "language" )
    ("org" "organization" )
    ("pm" "parameter" )
    ("prog" "programing" )
    ("q" "question" )
    ("ai" "artificial intelligence" )

    ;; tech company
    ("gg" "Google" )
    ("ms" "Microsoft" )
    ("msw" "Microsoft Windows" )
    ("osx" "OS X" )
    ("pp" "PayPal" )
    ("fb" "Facebook" )
    ("yt" "YouTube" )
    ("ff" "Firefox" )
    ("win" "Windows" )

    ;; programing
    ("faq" "frequently asked questions" )
    ("fs" "fullscreen" )
    ("ipa" "IP address" )
    ("mac" "Mac" )

    ("sj" "San Jose" )
    ("sf" "San Francisco" )

    ("subdir" "sub-directory" )
    ("subdirs" "sub-directories" )

    ("cli" "command line interface" )

    ("db" "database" )
    ("def" "definition" )
    ("df" "different" )
    ("dfc" "difference" )
    ("doc" "documentation" )
    ("dt" "data type" )
    ("el" "emacs lisp" )
    ("em" "emacs" )

    ("gui" "graphical user interface" )
    ("js" "JavaScript" )
    ("lib" "library" )
    ("libs" "libraries" )
    ("math" "mathematics" )
    ("md" "metadata" )
    ("os" "operating system" )

    ("sc" "source code" )

    ("temp" "temperature" )
    ("va" "variable" )
    ("vas" "variables" )

    ;; programing
    ("eq" "==" )
    ("eqq" "===" )
    ("r" "return" )

    ;; regex
    ("xaz" "\\([A-Za-z0-9]+\\)" )

    ;; unicode
    ("md" "—" )
    ("uascii" "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~" )

    ("bu" "•" )
    ("catface" "😸" )
    ("hearts" "♥💕💓💔💖💗💘💝💞💟💙💚💛💜" )
    ("ra" "→" )

    ;; code
    ("lgd" "LogDebug( \"\");" )
    ("lgda" "LogDebugArgs( \"\", );" )
    ("utf8" "-*- coding: utf-8 -*-" )

    ;;
    ))

(set-default 'abbrev-mode t)

;;;;Set the directory abbreviations are to be set in, and have abbrevs
;;;;auto-save itself and not ask questions.
(setq abbrev-file-name            
      "~/.emacs.d/my-lisp/abbrev_defs")   
(add-hook 'text-mode-hook (lambda () (abbrev-mode 1)))
(setq save-abbrevs t)
;;;(setq save-abbrevs nil)

;;;;This enables abbrev-sort. When abbreviations are editted,
;;;;they will be auto-sorted for ease of editting.
;;;;http://www.eskimo.com/~seldon/abbrev-sort.el
;(autoload 'abbrev-sort-mode "~/.emacs.d/abbrev-sort.el" "sorting abbrevs to make things easier." t)
;(abbrev-sort-mode 1)
