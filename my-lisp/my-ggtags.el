;;;; Add ggtags for GNU GLOBAL
;;;;
(require 'ggtags)

;;;; ggtags built-in key maps
;;;;
;;(define-key m (kbd "M-DEL") 'ggtags-delete-tags)
;;(define-key m "\M-p" 'ggtags-prev-mark)
;;(define-key m "\M-n" 'ggtags-next-mark)
;;(define-key m "\M-f" 'ggtags-find-file)
;;(define-key m "\M-o" 'ggtags-find-other-symbol)
;;(define-key m "\M-g" 'ggtags-grep)
;;(define-key m "\M-i" 'ggtags-idutils-query)
;;(define-key m "\M-b" 'ggtags-browse-file-as-hypertext)
;;(define-key m "\M-k" 'ggtags-kill-file-buffers)
;;(define-key m "\M-h" 'ggtags-view-tag-history)
;;(define-key m "\M-j" 'ggtags-visit-project-root)
;;(define-key m "\M-/" 'ggtags-view-search-history)
;;(define-key m (kbd "M-SPC") 'ggtags-save-to-register)
;;(define-key m (kbd "M-%") 'ggtags-query-replace)
;;(define-key m "\M-?" 'ggtags-show-definition)
;;(define-key map "\M-." 'ggtags-find-tag-dwim)
;;(define-key map (kbd "M-]") 'ggtags-find-reference)
;;(define-key map (kbd "C-M-.") 'ggtags-find-tag-regexp)


;;;
;;; use gtags.
;;;
;;; Leave below commented out to use built-in key mapping for the
;;; TAGS file built with exuberant_ctags.
;;;;

;;;; still need this?
;;(global-set-key "\M-." 'ggtags-find-tag-dwim)
;;(global-set-key "\M-," 'gtags-find-tag-other-window)
;;(global-set-key "\M-;" 'ggtags-find-tag-continue)
;;(global-set-key "\M-*" 'ggtags-view-tag-history)

; I'd like a key mapping similar to cscope.
; So, all gtags keys are prefixed with "\C-cg"
;
;(define-key gtags-mode-map "\C-cgh" 'gtags-display-browser)
;(define-key gtags-mode-map "\C-cg]" 'ggtags-find-tag-dwim)
;(define-key gtags-mode-map "\C-cgt" 'gtags-pop-stack)
;(define-key gtags-mode-map "\C-cgf" 'gtags-find-file)
;(define-key gtags-mode-map "\C-cgg" 'gtags-find-with-grep)
;(define-key gtags-mode-map "\C-cgI" 'gtags-find-with-idutils)
;(define-key gtags-mode-map "\C-cgs" 'gtags-find-symbol)
;(define-key gtags-mode-map "\C-cgr" 'gtags-find-rtag)
;(define-key gtags-mode-map "\C-cgt" 'gtags-find-tag)
;(define-key gtags-mode-map "\C-cgv" 'gtags-visit-rootdir)

; reserve the key maps to built-in functions
;(define-key gtags-select-mode-map "\M-." 'gtags-select-tag)
;(define-key gtags-select-mode-map "\M-*" 'gtags-pop-stack)
;(define-key gtags-mode-map "\C-cgl" 'gtags-select-tag)
;(define-key gtags-mode-map "\C-cgp" 'gtags-pop-stack)

;;;; Add GNU GLOBAL ggtags mode to c, c++
;;;;
;;;(setq 
;;; c-mode-hook '(lambda () (ggtags-mode 1))
;;; c++-mode-hook '(lambda () (ggtags-mode 1))
;;;)

(add-hook 'c-mode-common-hook
          (lambda ()
            (when (derived-mode-p 'c-mode 'c++-mode 'java-mode)
              (ggtags-mode 1))))
