;; -*- Chinese-*-
;;;; file: my-chinese.el
;;;; this is my basic chinese setup. Always load it if enable chinese in Windows.
;;;; this set default input method to 注音, "chinese-zozy"
;;;; use C-\ to switch between English and it.
;;;;
;;;; Also load "my-chinese-pyim.el" to enable pyim, 拼音
;;;;

;;;;
;;;; Windows下Emacs的shell-mode乱码解决
;;;; [ from google: Windows 下 Emacs24 的 shell-mode 中文乱码解决 ]
;;;;
;;;; 设置编辑环境
;;; 设置为中文简体语言环境
;;(set-language-environment 'Chinese-GB)
;;; 设置为中文繁体语言环境
(set-language-environment 'Chinese-EUC-TW)
;;(set-language-environment 'Chinese-CNS)
;;(set-language-environment 'Chinese-BIG5)

;; 设置emacs 使用 utf-8
(setq locale-coding-system 'utf-8)
;; 设置键盘输入时的字符编码
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
;; 文件默认保存为 utf-8
(set-buffer-file-coding-system 'utf-8)
(set-default buffer-file-coding-system 'utf8)
(set-default-coding-systems 'utf-8)
;; 解决粘贴中文出现乱码的问题
(set-clipboard-coding-system 'utf-8)
;; 终端中文乱码
(set-terminal-coding-system 'utf-8)
(modify-coding-system-alist 'process "*" 'utf-8)
(setq default-process-coding-system '(utf-8 . utf-8))
;; 解决文件目录的中文名乱码
(setq-default pathname-coding-system 'utf-8)
(set-file-name-coding-system 'utf-8)
;;;; Coding system names for 中文繁体:
;;;;   utf-8,
;;;;   chinese-big5 or bigs
;;;;   euc-tw or euc-taiwan
;;;;   chinese-big5-hkscs or big5-hkscs or cn-big5-hkscs
;;;;  
;;;;  
;;;;  
;; 解决 Shell Mode(cmd) 下中文乱码问题
;;(defun change-shell-mode-coding ()
;;  (progn
;;    (set-terminal-coding-system 'gbk)
;;    (set-keyboard-coding-system 'gbk)
;;    (set-selection-coding-system 'gbk)
;;    (set-buffer-file-coding-system 'gbk)
;;    (set-file-name-coding-system 'gbk)
;;    (modify-coding-system-alist 'process "*" 'gbk)
;;    (set-buffer-process-coding-system 'gbk 'gbk)
;;    (set-file-name-coding-system 'gbk)))

(defun change-shell-mode-coding ()
  (progn
    (set-terminal-coding-system 'euc-tw)
    (set-keyboard-coding-system 'euc-tw)
    (set-selection-coding-system 'euc-tw)
    (set-buffer-file-coding-system 'euc-tw)
    (set-file-name-coding-system 'euc-tw)
    (modify-coding-system-alist 'process "*" 'euc-tw)
    (set-buffer-process-coding-system 'euc-tw 'euc-tw)
    (set-file-name-coding-system 'euc-tw)))

(add-hook 'shell-mode-hook 'change-shell-mode-coding)
(autoload 'ansi-color-for-comint-mode-on "ansi-color" nil t)
(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)

;;;; set default to emacs built-in 注音
;;;; use C-\ to switch
(custom-set-variables '(default-input-method "chinese-zozy"))

;;;; Use M-x set-input-method <RET> pyim <RET> to select 拼音

;;;;
;;;; below is from another source.
;;;; so turn it off for now.
;;;;
;;(set-keyboard-coding-system 'chinese-iso-8bit)   ; input
;;(set-selection-coding-system 'chinese-iso-8bit)  ; copy/paste
;;(setq w32-enable-synthesized-fonts t)
;;(if (not (member '("-*-courier new-normal-r-*-*-13-*-*-*-c-*-fontset-chinese"
;;   . "fontset-chinese") fontset-alias-alist))
;;    (progn
;;        (create-fontset-from-fontset-spec ; chinese fontset
;;        "-*-Courier New-normal-r-*-*-14-*-*-*-c-*-fontset-chinese,
;;         chinese-gb2312:-*-MS Song-normal-r-*-*-12-*-*-*-c-*-gb2312*-*,
;;         chinese-big5-1:-*-MingLiU-normal-r-*-*-12-*-*-*-c-*-big5*-*,
;;         chinese-big5-2:-*-MingLiU-normal-r-*-*-12-*-*-*-c-*-big5*-*" t)
;;        
;;         (setq default-frame-alist
;;            (append
;;             '((font . "fontset-chinese"))
;;             default-frame-alist))
;;    )
;;)
