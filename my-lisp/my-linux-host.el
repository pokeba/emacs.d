
;;;;
;;;; emacs lisp functions for my linux systems
;;;;

;; initial window
(setq initial-frame-alist '( (width . 100) ; character
                             (height . 62) ; lines
                             ))

;; default/sebsequent window
(setq default-frame-alist '( (width . 100) ; character
                             (height . 62) ; lines
                             ))

(setq deft-directory "~/Dropbox/notes/dev/pub")
;;; "C-c 0" always go back to default deft directory.
(global-set-key (kbd "C-c 0")
                (lambda () (interactive) (bjm-deft "~/Dropbox/notes")))
(global-set-key (kbd "C-c 1")
            (lambda () (interactive) (bjm-deft "~/Dropbox/notes/dev")))
(global-set-key (kbd "C-c 2")
                (lambda () (interactive) (bjm-deft "~/Dropbox/notes/dev/pub")))
(global-set-key (kbd "C-c 3")
                (lambda () (interactive) (bjm-deft "~/Dropbox/notes/dev/sde")))
(global-set-key (kbd "C-c 5")
                (lambda () (interactive) (bjm-deft "~/Dropbox/notes/hai")))
