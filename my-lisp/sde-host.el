;;;;
;;;; emacs lisp functions for sde only
;;;;

;;;; ----- deft directory for no dropbox -----
;;;

;; initial window
(setq initial-frame-alist '( (width . 96) ; character
                             (height . 58) ; lines
                             ))

;; default/sebsequent window
(setq default-frame-alist '( (width . 96) ; character
                             (height . 58) ; lines
                             ))

(setq deft-directory "~/notes/dev")
;;; "C-c 0" always go back to default deft directory.
(global-set-key (kbd "C-c 0")
                (lambda () (interactive) (bjm-deft "~/notes")))
(global-set-key (kbd "C-c 1")
            (lambda () (interactive) (bjm-deft "~/notes/dev")))
(global-set-key (kbd "C-c 2")
                (lambda () (interactive) (bjm-deft "~/notes/dev/pub")))
(global-set-key (kbd "C-c 3")
                (lambda () (interactive) (bjm-deft "~/notes/dev/sde")))
(global-set-key (kbd "C-c 5")
                (lambda () (interactive) (bjm-deft "~/notes/hai")))
