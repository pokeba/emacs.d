;;;; file: my-chinese-pyim.el
;;;;
;;;; This 拼音 method.
;;;; load this file to enable pyim 拼音
;;;;
;;;;
;;;; To switch to this input method, do:
;;;;    M-x set-input-method <RET> pyim <RET>
;;;; To switch back/forward
;;;;    C-/

;;;; pyim
;;;; https://github.com/tumashu/pyim#org4f5dee3
;;;;
;;(require 'pyim)
;;(require 'pyim-basedict) ; 拼音词库设置，五笔用户 *不需要* 此行设置
;;(pyim-basedict-enable)   ; 拼音词库，五笔用户 *不需要* 此行设置
;;(setq default-input-method "pyim")
;;(setq pyim-page-tooltip 'popup)
;;(setq pyim-page-tooltip 'pos-tip)

;;;; setup from the author of pyim
;;;;
(require 'pyim)
(require 'pyim-basedict) ; 拼音词库设置，五笔用户 *不需要* 此行设置
(pyim-basedict-enable)   ; 拼音词库，五笔用户 *不需要* 此行设置
(setq default-input-method "pyim")
(setq pyim-default-scheme 'quanpin)
;; 开启拼音搜索功能
(setq pyim-isearch-enable-pinyin-search t)

;; 使用 pupup-el 来绘制选词框
(setq pyim-page-tooltip 'popup)

;;;; set pyim dictionaries. For example,
;;;(setq pyim-dicts
;;;      '((:name "dict1" :file "/path/to/pyim-dict1.pyim")
;;;        (:name "dict2" :file "/path/to/pyim-dict2.pyim")))
;;; 只有 :file 是 必须 设置的。
;;; 必须使用词库文件的绝对路径。
;;; 词库文件的编码必须为 utf-8-unix，否则会出现乱码。
;;;
(setq pyim-dicts
      '((:name "pyim-bigdict" :file "~/.emacs.d/idct/pyim-bigdict.pyim")
        ))

;;;; Chinese-pyim
;;(require 'chinese-pyim)
;;qq(setq default-input-method "chinese-pyim")

(require 'cnfonts)
;;; before update for Chinese
;;;
;; if comment below line, then emacs staysin default English environment.
;;(set-language-environment "Chinese-GB18030")
(set-default-coding-systems 'utf-8)
(prefer-coding-system 'utf-8)

(if (not (member '("-*-courier new-normal-r-*-*-13-*-*-*-c-*-fontset-chinese"
   . "fontset-chinese") fontset-alias-alist))
    (progn
      (create-fontset-from-fontset-spec
       "-*-courier new-normal-r-*-*-13-*-*-*-c-*-fontset-chinese,
      chinese-gb2312:-*-MS Song-normal-r-*-*-16-*-*-*-c-*-gb2312*-*,
      chinese-big5-1:-*-MingLiU-normal-r-*-*-16-*-*-*-c-*-big5*-*,
      chinese-big5-2:-*-MingLiU-normal-r-*-*-16-*-*-*-c-*-big5*-*" t)

      (setq default-frame-alist
            (append
             '((font . "fontset-chinese"))
             default-frame-alist))
      )
  )

;;; new settings for Chinese
;;;
;;(set-buffer-file-coding-system 'utf-8-unix)
;;(set-clipboard-coding-system 'utf-8-unix)
;;(set-file-name-coding-system 'utf-8-unix)
;;(set-keyboard-coding-system 'utf-8-unix)
;;(set-next-selection-coding-system 'utf-8-unix)
;;(set-selection-coding-system 'utf-8-unix)
;;(set-terminal-coding-system 'utf-8-unix)
;;(setq locale-coding-system 'utf-8)

;;(setq default-frame-alist (font . "WenQuanYi Micro Hei Mono"))

;;;; Use M-x set-input-method <RET> pyim <RET> to select 拼音

