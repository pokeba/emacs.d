;; -*- Chinese-*-
;;;; file: my-chinese.el
;;;; this is my basic chinese setup. Always load it if enable chinese in Windows.
;;;; this set default input method to 注音, "chinese-zozy"
;;;; use C-\ to switch between English and it.
;;;;
;;;; Also load "my-chinese-pyim.el" to enable pyim, 拼音
;;;;

;;;;
;;;; Windows下Emacs的shell-mode乱码解决
;;;; [ from google: Windows 下 Emacs24 的 shell-mode 中文乱码解决 ]
;;;;
;;;; 设置编辑环境
;;; 设置为中文简体语言环境
;;(set-language-environment 'Chinese-GB)
;;; 设置为中文繁体语言环境
;;(set-language-environment 'Chinese-EUC-TW)
;;(set-language-environment 'Chinese-CNS)
;;(set-language-environment 'Chinese-BIG5)



;;;; set default to emacs built-in 注音
;;;; use C-\ to switch
(custom-set-variables '(default-input-method "chinese-zozy"))

(set-language-environment 'Chinese-GB)
(set-keyboard-coding-system 'chinese-iso-8bit)
(set-terminal-coding-system 'chinese-iso-8bit)
(set-clipboard-coding-system 'chinese-iso-8bit) 
(set-selection-coding-system 'chinese-iso-8bit)

;;;; Use M-x set-input-method <RET> pyim <RET> to select 拼音

;;;;
;;;; below is from another source.
;;;; so turn it off for now.
;;;;
;;(set-keyboard-coding-system 'chinese-iso-8bit)   ; input
;;(set-selection-coding-system 'chinese-iso-8bit)  ; copy/paste
;;(setq w32-enable-synthesized-fonts t)
;;(if (not (member '("-*-courier new-normal-r-*-*-13-*-*-*-c-*-fontset-chinese"
;;   . "fontset-chinese") fontset-alias-alist))
;;    (progn
;;        (create-fontset-from-fontset-spec ; chinese fontset
;;        "-*-Courier New-normal-r-*-*-14-*-*-*-c-*-fontset-chinese,
;;         chinese-gb2312:-*-MS Song-normal-r-*-*-12-*-*-*-c-*-gb2312*-*,
;;         chinese-big5-1:-*-MingLiU-normal-r-*-*-12-*-*-*-c-*-big5*-*,
;;         chinese-big5-2:-*-MingLiU-normal-r-*-*-12-*-*-*-c-*-big5*-*" t)
;;        
;;         (setq default-frame-alist
;;            (append
;;             '((font . "fontset-chinese"))
;;             default-frame-alist))
;;    )
;;)
