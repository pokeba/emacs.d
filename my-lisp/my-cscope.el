;;;;Add cscope
;;;;
;;;;(require 'xcscope)

;;;;(setq cscope-do-not-update-database t)

;;;; Below are key mappings for cscope
;;;;
(global-set-key  "\C-css" 'cscope-find-this-symbol)
(global-set-key  "\C-csd" 'cscope-find-global-definition)
(global-set-key  "\C-csg" 'cscope-find-global-definition)
(global-set-key  "\C-csG" 'cscope-find-global-definition-no-prompting)
(global-set-key  "\C-csc" 'cscope-find-functions-calling-this-function)
(global-set-key  "\C-csC" 'cscope-find-called-functions)
(global-set-key  "\C-cst" 'cscope-find-this-text-string)
(global-set-key  "\C-cse" 'cscope-find-egrep-pattern)
(global-set-key  "\C-csf" 'cscope-find-this-file)
(global-set-key  "\C-csi" 'cscope-find-files-including-file)

; (global-set-key  "\C-csb" 'cscope-display-buffer)
(global-set-key  "\C-csB" 'cscope-display-buffer-toggle)
(global-set-key  "\C-csn" 'cscope-next-symbol)
(global-set-key  "\C-csN" 'cscope-next-file)
(global-set-key  "\C-csp" 'cscope-prev-symbol)
(global-set-key  "\C-csP" 'cscope-prev-file)
(global-set-key  "\C-csu" 'cscope-pop-mark)
  ;; ---
(global-set-key  "\C-csa" 'cscope-set-initial-directory)
(global-set-key  "\C-csA" 'cscope-unset-initial-directory)
