;;;; Add gtags for GNU GLOBAL
;;;;
(require 'gtags)
(autoload 'gtags-mode "gtags" "" t)

;;;; Below are key mapping for GNU GLOBAL gtags
;;;;
(defun ww-next-gtag ()
  "Find next matching tag, for GTAGS."
  (interactive)
  (let ((latest-gtags-buffer
         (car (delq nil  (mapcar (lambda (x) (and (string-match "GTAGS SELECT" (buffer-name x)) (buffer-name x)) )
                          (buffer-list))
              ))))
    (cond (latest-gtags-buffer
           (switch-to-buffer latest-gtags-buffer)
           (forward-line)
           (gtags-select-it nil))
          ) 
  )
)

;;;
;;; use gtags.
;;;
;;; Leave below commented out to use built-in key mapping for the
;;; TAGS file built with exuberant_ctags.
;;;;

(global-set-key "\M-." 'gtags-find-tag)
(global-set-key "\M-," 'gtags-find-tag-other-window)
(global-set-key "\M-;" 'gtags-find-tag-from-here)
;;;(global-set-key "\M-," 'ww-next-gtag)
(global-set-key "\M-*" 'gtags-pop-stack)

; If you hope old style key assignment. Please include following code
; to your $HOME/.emacs:
; Note "\eh" is "\M-h", "\C-]" is "Ctrl-]".
;
;(define-key gtags-mode-map "\eh" 'gtags-display-browser)
;(define-key gtags-mode-map "\C-]" 'gtags-find-tag-from-here)
;(define-key gtags-mode-map "\C-t" 'gtags-pop-stack)
;(define-key gtags-mode-map "\el" 'gtags-find-file)
;(define-key gtags-mode-map "\eg" 'gtags-find-with-grep)
;(define-key gtags-mode-map "\eI" 'gtags-find-with-idutils)
;(define-key gtags-mode-map "\es" 'gtags-find-symbol)
;(define-key gtags-mode-map "\er" 'gtags-find-rtag)
;(define-key gtags-mode-map "\et" 'gtags-find-tag)
;(define-key gtags-mode-map "\ev" 'gtags-visit-rootdir)
;
; I'd like a key mapping similar to cscope.
; So, all gtags keys are prefixed with "\C-cg"
;
(define-key gtags-mode-map "\C-cgh" 'gtags-display-browser)
(define-key gtags-mode-map "\C-cg]" 'gtags-find-tag-from-here)
(define-key gtags-mode-map "\C-cgt" 'gtags-pop-stack)
(define-key gtags-mode-map "\C-cgf" 'gtags-find-file)
(define-key gtags-mode-map "\C-cgg" 'gtags-find-with-grep)
(define-key gtags-mode-map "\C-cgI" 'gtags-find-with-idutils)
(define-key gtags-mode-map "\C-cgs" 'gtags-find-symbol)
(define-key gtags-mode-map "\C-cgr" 'gtags-find-rtag)
(define-key gtags-mode-map "\C-cgt" 'gtags-find-tag)
(define-key gtags-mode-map "\C-cgv" 'gtags-visit-rootdir)

; reserve the key maps to built-in functions
;(define-key gtags-select-mode-map "\M-." 'gtags-select-tag)
;(define-key gtags-select-mode-map "\M-*" 'gtags-pop-stack)
(define-key gtags-mode-map "\C-cgl" 'gtags-select-tag)
(define-key gtags-mode-map "\C-cgp" 'gtags-pop-stack)

;;;; Add GNU GLOBAL gtags mode to c, c++
;;;;
(add-hook 'c-mode-common-hook
          (lambda ()
            (when (derived-mode-p 'c-mode 'c++-mode 'java-mode)
              (gtags-mode 1))))
