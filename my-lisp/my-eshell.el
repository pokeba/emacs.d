;;;; A better interface to eshell history with C-c h and C-c C-h
(add-hook 'eshell-mode-hook
          (lambda ()
            (local-set-key (kbd "C-c h")
                           (lambda ()
                             (interactive)
                             (insert
                              (ido-completing-read "Eshell history: "
                                                   (delete-dups
                                                    (ring-elements eshell-history-ring))))))
            (local-set-key (kbd "C-c C-h") 'eshell-list-history)))

;;;;
;;;; use dired's guess shell command functions:
;;;;
(defun eshell/dwim (&rest args)
  "Use dired's guess alist to execute command on file"
  (if (not args) (message "No argument given, did you mean: 'sudo rm -f /'?")
    (let* ((guess (dired-guess-default args))
           (candidate (if (listp guess) (car guess) guess)))
      (eshell-smart-maybe-jump-to-end)
      (insert-and-inherit (concat candidate " " (car args) " &")))))

(defun eshell/dwim! (&rest args)
  "Use dired's guess alist to execute command on file immediately"
  (apply 'eshell/dwim args)
  (eshell-send-input))
