;;;;.emacs.el --- my own .emacs

;;;;Copyright (C) 2006 by maru dubshinki
;;;;
;;;;Author: maru dubshinki <marudubshinki0@gmail.com>
;;;;License: BSD license, minus advertising clause.
;;;;Where: http://en.wikipedia.org/wiki/User:Marudubshinki/.emacs
;;;;Keywords: local,customization

;;;; Commentary
;;;;This is divided into three sections. The first section sets and modifies
;;;;Emacs directly, invoking and tweaking packages and settings that
;;;;come with your vanilla Emacs CVS, such as scrolling or changing 
;;;;key bindings. The second section loads all my downloaded elisp files, 
;;;;and sets them up. The third section contains the customize settings.
;;;;That's the smallest section, since I did most of this by hand.


;;;;;;;;;;;;Emacs customizations in general;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;This is generally for everything that can be easily done with defuns
;;;or for customizations to emacs proper, not merely adding in new stuff.
;;;Tested with Debian's snapshot package; should largely work on 21 as well.

;; load emacs 24's package system. Add MELPA repository.
(when (>= emacs-major-version 24)
  (require 'package)
  (add-to-list
   'package-archives
   ;; '("melpa" . "http://stable.melpa.org/packages/") ; many packages won't show if using stable
   '("melpa" . "http://melpa.milkbox.net/packages/")
   t))

;;;;Add the directory I keep all my special .el files in into the
;;;;default load path.

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(setq load-path (cons "~/.emacs.d/my-lisp/" load-path))
(setq load-path (cons "~/.emacs.d/lisp/" load-path))

(load "replace")
(load "xah-lisp")
(load "my-eshell")
(load "my-keybindings")

;;;;
;;;; enable this only when want to change some GUI colors/window sizes,...
;;;; comment it out after done.
;;(load "my-face-setup")

;;;; this is my basic chinese setup. Always load it if enable chinese in Windows.
;;;; this set default input method to 注音, "chinese-zozy"
;;;; use C-\ to switch between English and it.
;;;(load "my-chinese-3")

;;;; load my-chinese-pyim.el to enable pyim 拼音
;;;;
;;;; To switch to this input method, do:
;;;;    M-x set-input-method <RET> pyim <RET>
;;;; To switch back/forward
;;;;    C-\
;;;;
;;;(load "my-chinese-pyim")

;;;; use my-chinese.el if only want emacs built-in 注音
;;(load "my-chinese")

(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8)
;;(prefer-coding-system 'utf-8)

;;(load "my-chi-3")

;;(load "my-fonts")

;; remember cursor position, for emacs 25.1 or later
(save-place-mode 1)

;; save/restore opened files and windows config
;; (desktop-save-mode 1) ; 0 for off

;;;; ----- set a default font -----
;;;;
(when (member "DejaVu Sans Mono" (font-family-list))
  (set-face-attribute 'default nil :font "DejaVu Sans Mono-10"))

;;;;
;;;; set default font in initial window and for any new window
;;;;

(cond
 ((string-equal system-type "windows-nt") ; Microsoft Windows
  (when (member "DejaVu Sans Mono" (font-family-list))
    (add-to-list 'initial-frame-alist '(font . "DejaVu Sans Mono-10"))
    (add-to-list 'default-frame-alist '(font . "DejaVu Sans Mono-10"))))
 ((string-equal system-type "darwin") ; Mac OS X
  (when (member "DejaVu Sans Mono" (font-family-list))
    (add-to-list 'initial-frame-alist '(font . "DejaVu Sans Mono-10"))
    (add-to-list 'default-frame-alist '(font . "DejaVu Sans Mono-10"))))
 ((string-equal system-type "gnu/linux") ; linux
  (when (member "DejaVu Sans Mono" (font-family-list))
    (add-to-list 'initial-frame-alist '(font . "DejaVu Sans Mono-10"))
    (add-to-list 'default-frame-alist '(font . "DejaVu Sans Mono-10")))))

;;; To enable abbrev mode, highlight below load line and
;;; do "M-x eval-region"
;;;(load "my-abbrev")

;;;;
;;;; el-get is an automatic emacs packages manager.
;;;; It's very powerful. But is is also very blind.
;;;; I think it is a big security hole that the
;;;; users blindly trust the emacs packages are all
;;;; safe and secure.
;;;; So this package should NEVER be enabled !!!
;;;; I leave this comments and code here for
;;;; future reference.
;;;;
;;;; I have also removed ~/.emacs.d/el-get directory.
;;;; This package should not be enabled!
;;;;
;;;; (add-to-list 'load-path "~/.emacs.d/el-get/el-get")
;;;; (unless (require 'el-get nil t) 
;;;;  (url-retrieve
;;;;   "https://github.com/dimitri/el-get/raw/master/el-get-install.el" 
;;;;   (lambda (s) (goto-char (point-max)) (eval-print-last-sexp))))
;;;;

;;; set indentation to always use space
(setq-default indent-tabs-mode nil)

(setq-default tab-width 4)
;;; set current buffer's tab char's display width to 4.
(setq tab-width 4)

;;; make tab key always call an indent command 
(setq-default tab-always-indent t)
;;;
;;; make tab key call indent command or insert <tab> depending
;;; on cursor position
;;; (setq-default tab-always-indent nil)
;;;
;;; make tab key do indent first then complete.
;;; (setq-default tab-always-indent 'complete)

(setq indent-line-function 'insert-tab)

(setq indent-tabs-mode nil)

(setq-default c-basic-offset 4)

;;;; set term-buffer-maximum-size to 0 meant infinite.
(add-hook 'term-mode-hook (lambda ()
                            (setq term-buffer-maximum-size 0 )))

(add-hook 'java-mode-hook (lambda ()
                            (setq c-basic-offset 4
                                  tab-width 4
                                  indent-tabs-mode nil)))

;;;
;;; enable smart tab
;;;
;;(smart-tabs-insinuate 'c)

;; This enables jump back to mark ring places
(setq set-mark-command-repeat-pop t)

;;; Make ibuffer default
(defalias 'list-buffers 'ibuffer)

;;; bash-completion.el defines dynamic completion hooks
;;; for shell-mode and shell-command prompts that are
;;; based on bash completion.
(autoload 'bash-completion-dynamic-complete 
  "bash-completion"
  "BASH completion hook")
(add-hook 'shell-dynamic-complete-functions
          'bash-completion-dynamic-complete)

;;; You can also force the above by
;;;
;;;(require 'bash-completion)
;;;(bash-completion-setup)

;;;;A fun startup message, somewhat reminiscent of "The Matrix: Reloaded"
(defun emacs-reloaded ()
  (animate-string (concat ";; Initialization successful. Welcome to "
  			  (substring (emacs-version) 0 16)
			  ".")
		  0 1)
  (newline-and-indent)  (newline-and-indent))
(add-hook 'after-init-hook 'emacs-reloaded) 

(add-hook 'java-mode-hook (lambda ()
                            (setq c-basic-offset 4
                                  tab-width 4
                                  indent-tabs-mode t)))

;; I want an easy command for opening new shells:
(defun named-shell (name)
  "Opens a new shell buffer with the given name in
asterisks (*name*) in the current directory and changes the
prompt to 'name>'."
  (interactive "sshell name: ")
  (pop-to-buffer (concat "*" name "*"))
  (unless (eq major-mode 'shell-mode)
    (shell (current-buffer))
    (sleep-for 0 200)
    (delete-region (point-min) (point-max))
    (comint-simple-send (get-buffer-process (current-buffer)) 
                        (concat "export PS1=\"\033[33m" name "\033[0m:\033[35m\\W\033[0m>\""))))

(defun named-eshell (name)
  "Opens a new eshell buffer with the given name in
asterisks (*name*) in the current directory and changes the
prompt to 'name>'."
  (interactive "seshell name: ")
  (setq eshell-buffer-name name)
  (pop-to-buffer (concat "*" name "*"))
  (unless (eq major-mode 'eshell-mode)
    (eshell)
    (sleep-for 0 200)
    (delete-region (point-min) (point-max))
    (comint-simple-send (get-buffer-process (current-buffer))
                        (concat "export PS1=\"\033[33m" name "\033[0m:\033[35m\\W\033[0m>\""))))

;; below command start a shell in a "terminal" (emacs term-mode).
;;
(defun named-term (buffer-name)
  "Start a terminal and rename buffer."
  (interactive "sbuffer name: ")
  (term "/bin/bash")
  (rename-buffer buffer-name t))

;;;; functions to move line up and down
;;;; cursor position wuillbe preserved.
;;;; source: https://www.emacswiki.org/emacs/MoveLine
;;;;
(defmacro save-column (&rest body)
  `(let ((column (current-column)))
     (unwind-protect
         (progn ,@body)
       (move-to-column column))))
(put 'save-column 'lisp-indent-function 0)

(defun move-line-up ()
  (interactive)
  (save-column
    (transpose-lines 1)
    (forward-line -2)))

(defun move-line-down ()
  (interactive)
  (save-column
    (forward-line 1)
    (transpose-lines 1)
    (forward-line -1)))

;;; custom-theme-directory is "~/.emacs.d"

;;;
(add-to-list 'custom-theme-load-path "~/.emacs.d/my-themes")
(load-theme 'blackboard t)

;; The value for font size is in 1/10pt, so 120 will give you 12pt, etc.
;; In many systems, the defaukt is just right.
;; So set this only when it is needed.
;;(set-face-attribute 'default nil :height 120)

;;;;I've seen this in a few other .emacs, and it doesn't seem to do
;;;;anything *bad*...
(random t) 

;;;;; set global mark ring entries
(setq global-mark-ring-max 100)

;;;;the bookmarks are saved every time I make or delete a bookmark.
(setq bookmark-save-flag 1)  


;;;; set tab stop to every 4 characters
(setq tab-stop-list (number-sequence 4 120 4))

(setq indent-tabs-mode nil)
(setq-default indent-tabs-mode nil)

;;;; org mode
(require 'org)
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(setq org-log-done t)
(setq org-agenda-files (list "~/org/work.org"
                             "~/org/home.org"))
(setq-default org-list-indent-offset 4)
(setq org-list-demote-modify-bullet '(("-"  . "+")
                                      ("+"  . "*")
                                      ("*"  . "-")
                                      ("1."  . "1)")
                                      ("1)"  . "1.")))
;;;
;;; This value set whether line wrapping.
;;; Set to nil so line will wrap in org-mode.
(setq org-startup-truncated nil)

;;;
;;; toggle line wrapping
;;; If toggle only within org mode:
(define-key org-mode-map "\C-c\C-w" 'toggle-truncate-lines)
;;; To toggle globally:
;;;(global-set-key "\C-c\C-w" 'toggle-truncate-lines)
;;;

;;;
;;; This will set the doc indent.
;;; For now, I'll use no ident.
;;;
;;;(with-eval-after-load 'org       
;;;  (setq org-startup-indented t) ; Enable `org-indent-mode' by default
;;;  (add-hook 'org-mode-hook #'visual-line-mode))

;;;; Use ggtags or gtags
;;;; 
;;;; I found the best way is to setup exubent ctags to build GTAGS as described
;;;; in ggtags web page. 
;;;; Then use gtags to access it.
;;;; So I get "M-," to find tags in other windows.
;;;; It also seems ctags' tags are more accurate.
;;;;
;;;(load "my-ggtags")
(load "my-gtags")

;;;; Need cscope ?
;;; (load "my-cscope")

(load "my-deft")

(require 'goto-last-change)

(global-set-key "\C-x\C-\\" 'goto-last-change)

;;;
;;; For my own systems
;;; Comment these section out in sde.
;;;
;;;; Note keys C-c 0, C-c 1, ... C-c 5 are used to deft mode which is defined in
;;;; my-linux-host.el and sde-host.el
;;;;
;;(load "my-linux-host")
(load "washibon-host")
;;(load "sde-host")
;;(load "home-wsl-host")
;;(load "my-symc-mac-host")

;; Emacs GNU ID utils interface
;; M-x gid to run the command
;; more info at http://www.gnu.org/software/idutils/manual/idutils.html
(autoload 'gid "idutils" nil t)

;;;; 
;;;; use ido or ivy mode
;;;;
;;;(load "my-ido")
;;;
(load "my-ivy")
;;; use helm
;;;(load "my-helm")

(load "my-markdown")

;;; Ignore the .aux extensions that TeX programs create 
(setq completion-ignored-extensions 
      (cons "*.aux" completion-ignored-extensions)) 

;;;;
;;;; smex
;;;;
(require 'smex) ; Not needed if you use package.el
;;;;(smex-initialize) ; Can be omitted. This might cause a (minimal) delay
                  ; when Smex is auto-initialized on its first run.

;;; with smex, M-x is replaced with smex
(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)
(global-set-key (kbd "C-c M-X") 'smex-update)
;; This is your old M-x.
(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)


;;;;
;; clipboard is the mechanism for X cut-copy-paste.
;; By default, x-select-enable-clipboard is 't'
;; this allow kill and yank to the clipboard.
;; set it to nil, then kill and yank do not use clipboard.
;;
;; (setq x-select-enable-clipboard nil)
;;
;;;;
;;;; Read emacs manual section: 12.3.1 Using the Clipboard
;;;; [ http://www.gnu.org/software/emacs/manual/html_node/emacs/Clipboard.html ]
;;;;
;;;;
;; (setq x-select-enable-primary t)
(setq mouse-drag-copy-region t)
(setq x-select-enable-clipboard-manager nil)

;;
;; save clipboard content to kill ring before replace its content.
;;
(setq save-interprogram-paste-before-kill t)
;;

;;;;Add my info
;;;;
(require 'info)
;;(setq Info-directory-list
;;      (cons (expand-file-name "~/info") Info-default-directory-list))

;;;; set default indent for c mode.
(setq c-default-style "stroustrup")

;;;;Ditto.
(setq hostname (getenv "HOSTNAME"))

;;;;Do without those obnoxious startup messages- the whole GNU Emacs logo
;;;;thing, that is.
(setq inhibit-splash-screen t) 

;;;;Enable the bell- but make it visible and not aural.
(setq visible-bell t)

;;;;Modify the mode-line as well. This is a cleaner setup than the 
;;;;default settings for the mode-line.
(setq mode-line-format
      '("-"
       mode-line-mule-info
       mode-line-modified
       mode-line-frame-identification
       mode-line-buffer-identification
       "  "
       global-mode-string
       "   %[(" mode-name mode-line-process minor-mode-alist "%n"
")%]--"
       (line-number-mode "L%l--")
       (column-number-mode "C%c--")
       (-3 . "%p")
       "-%-"))

;;;;While we are getting rid of stuff, let's get rid of the buttons.
;;;;The menu is useful, so we will keep that, but the buttons are 
;;;;redundant with the menu options, and I know most of the keybindings 
;;;;for the buttons, anyway.
(tool-bar-mode -1)

;;;;Rearrange the menubars, so it goes tools buffers help.
(setq menu-bar-final-items '(tools buffer help-menu))

;;;;I can't stop killing! Shut off message buffer. Note - if you need
;;;;to debug emacs, comment these out so you can see what's going on.
(setq message-log-max nil)
(kill-buffer "*Messages*")

;;;;Let's not have too-tiny windows.
(setq window-min-height 3)

;;;;Provide a useful error trace if loading this .emacs fails.
(setq debug-on-error t)

;;;;This sets garbage collection to hundred times of the default. 
;;;;Supposedly significantly speeds up startup time. (Seems to work
;;;;for me,  but my computer is pretty modern. Disable if you are on 
;;;;anything less than 1 ghz).
(setq gc-cons-threshold 50000000)

;;;;Require C-x C-c prompt. I've closed too often by accident.
;;;;http://www.dotemacs.de/dotfiles/KilianAFoth.emacs.html
(global-set-key [(control x) (control c)] 
  (function 
   (lambda () (interactive) 
     (cond ((y-or-n-p "Quit? ")
            (save-buffers-kill-emacs))))))

;;;;C-x k is a command I use often, but C-x C-k (an easy mistake) is
;;;;bound to nothing!
;;;;Set C-x C-k to same thing as C-x k.
(global-set-key "\C-x\C-k" 'kill-this-buffer)

;; cycle through amounts of spacing
;; Note M-SPC is already used by other linux app keybinding.
;;(global-set-key (kbd "M-SPC") 'cycle-spacing)

;;;;Don't echo passwords when communicating with interactive programs-
;;;;basic security.
(add-hook 'comint-output-filter-functions
'comint-watch-for-password-prompt)

;;;;Use ANSI colors within shell-mode
(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)

;;;;Enable narrowing of regions  
(put 'narrow-to-region 'disabled nil)

;;;;Allow a command to erase an entire buffer
(put 'erase-buffer 'disabled nil)

;;;;Disable over-write mode! Never good! Pain in the posterior!
(put 'overwrite-mode 'disabled t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; commented out old code
;;;;
;;;;Using cursor color to indicate some modes. If you sometimes 
;;;;find yourself inadvertently overwriting some text because you 
;;;;are in overwrite mode but you didn't expect so, this might prove 
;;;;as useful to you as it is for me. It changes cursor color to 
;;;;indicate read-only, insert and overwrite modes:
;;(setq hcz-set-cursor-color-color "")
;;(setq hcz-set-cursor-color-buffer "")
;;(defun hcz-set-cursor-color-according-to-mode ()
;;  "change cursor color according to some minor modes."
;;  ;;set-cursor-color is somewhat costly, so we only call it when
;;  ;;needed:
;;  (let ((color
;;	 (if buffer-read-only "black"
;;	   (if overwrite-mode "red"
;;	     "blue"))))
;;    (unless (and
;;	     (string= color hcz-set-cursor-color-color)
;;	     (string= (buffer-name) hcz-set-cursor-color-buffer))
;;      (set-cursor-color (setq hcz-set-cursor-color-color color))
;;      (setq hcz-set-cursor-color-buffer (buffer-name)))))
;;
;;(add-hook 'post-command-hook 'hcz-set-cursor-color-according-to-mode)
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;(set-cursor-color "#ffffff")

(defvar blink-cursor-colors (list  "#92c48f" "#6785c5" "#be369c" "#d9ca65")
  "On each blink the cursor will cycle to the next color in this list.")

(setq blink-cursor-count 0)
(defun blink-cursor-timer-function ()
  "Zarza wrote this cyberpunk variant of timer `blink-cursor-timer'. 
Warning: overwrites original version in `frame.el'.

This one changes the cursor color on each blink. Define colors in `blink-cursor-colors'."
  (when (not (internal-show-cursor-p))
    (when (>= blink-cursor-count (length blink-cursor-colors))
      (setq blink-cursor-count 0))
    (set-cursor-color (nth blink-cursor-count blink-cursor-colors))
    (setq blink-cursor-count (+ 1 blink-cursor-count))
    )
  (internal-show-cursor nil (not (internal-show-cursor-p)))
  )

;;;;Change pasting behavior. Normally, it pastes where the mouse 
;;;;is at, which is not necessarily where the cursor is. This changes 
;;;;things so all pastes, whether they be middle-click or C-y or menu, 
;;;;all paste at the cursor.
(setq mouse-yank-at-point t) 

;;;;Activate font-lock-mode. Syntax coloring, yay! 
(global-font-lock-mode t)

(require 'paren)

;;;; commented out due to error
;;;;While we are at it, always flash for parens.
(show-paren-mode 1)
;;;; There are two styles for highlighting brackets
;;;; This highlights brackets
(setq show-paren-style 'parenthesis)
;;;; This highlights entire expression
;;;;(setq show-paren-style 'expression)

(defun enable-paren-expression ()
  (interactive)
  (message "Show matching paren with expression")
  (setq show-paren-style 'expression) )

(defun disable-paren-expression ()
  (interactive)
  (message "Show matching paren with parenthesis only")
  (setq show-paren-style 'parenthesis) )

;; enable abbrev mode
(defun enable-abbrev ()
  (interactive)
  (message "Enable abbrev mode")
  (load "my-abbrev.el") )

;;;;(show-paren-overlay 1)
;;;;(setq show-paren-delay 0)

(set-background-color "black")

;;;;The autosave is typically done by keystrokes, but I'd like to save
;;;;after a certain amount of time as well.
(setq auto-save-timeout 1800)
;;;; derfault auto-save every 300 key strokes.
(setq auto-save-interval 300)

;;;;Change backup behavior to save in a directory, not in a miscellany
;;;;of files all over the place.
(setq
 backup-by-copying t      ; don't clobber symlinks
 backup-directory-alist
 '(("." . "~/.saves"))    ; don't litter my fs tree
 delete-old-versions t
 kept-new-versions 6
 kept-old-versions 2
 version-control t)       ; use versioned backups


;;;;Don't bother entering search and replace args if the buffer is
;;;;read-only. Duh.
(defadvice query-replace-read-args (before barf-if-buffer-read-only
activate)
  "Signal a `buffer-read-only' error if the current buffer is
read-only."
  (barf-if-buffer-read-only))

;;;;This starts up a server automatically, so it will be running to 
;;;;accept things like wikipedia articles.
;(server-start)

;;;;Call a function which will have the time displayed in the modeline
(display-time)

;;;;I don't find fundamental mode very useful. Things generally have a
;;;;specific mode, or they're text.
(setq major-mode 'text-mode)

;;;;Show column number in mode line
(setq column-number-mode t)

;;;;Turn off the status bar if we're not in a window system
(menu-bar-mode (if window-system 1 -1))

;;;;W3m browser- doesn't work with my Emacs CVS.
;(require 'w3m-load)

;; (require 'ssh)

;;;;Answer y or n instead of yes or no at minibar prompts.
(defalias 'yes-or-no-p 'y-or-n-p)

;;;; keyboard scroll one line at a time
;;(setq scroll-step 1)

;;;;Fix the whole huge-jumps-scrolling-between-windows nastiness
(setq scroll-conservatively 5)

;;;; scroll one line at a time when cursor pass the top or bottom
;;(setq scroll-conservatively 10000)

;;;;"Don't hscroll unless needed"- ? More voodoo lisp.
(setq hscroll-margin 1)

(setq auto-window-vscroll nil)

;;;;What it says. Keeps the cursor in the same relative row during
;;;;pgups and dwns.
(setq scroll-preserve-screen-position t)

 ;;;;;Accelerate the cursor when scrolling.
(load "accel" t t)

;;;;Start scrolling when 2 lines from top/bottom
(setq scroll-margin 2)

;;;;;Push the mouse out of the way when the cursor approaches.
(mouse-avoidance-mode 'jump)

;;;;Make cursor stay in the same column when scrolling using pgup/dn.
;;;;Previously pgup/dn clobbers column position, moving it to the
;;;;beginning of the line.
;;;;http://www.dotemacs.de/dotfiles/ElijahDaniel.emacs.html
(defadvice scroll-up (around ewd-scroll-up first act)
  "Keep cursor in the same column."
  (let ((col (current-column)))
    ad-do-it
    (move-to-column col)))

(defadvice scroll-down (around ewd-scroll-down first act)
  "Keep cursor in the same column."
  (let ((col (current-column)))
    ad-do-it                          
    (move-to-column col))) 

;;;SavePlace- this puts the cursor in the last place you editted
;;;a particular file. This is very useful for large files.
(require 'saveplace)
(setq-default save-place t)

;;;I use sentences.  Like this.
(setq sentence-end-double-space t)

;;;;Supposedly, this turns on flyspell mode for text editing.
;;;(dolist (hook '(text-mode-hook))
;;;  (add-hook hook (lambda () (flyspell-mode 1))))
;;;(dolist (hook '(change-log-mode-hook log-edit-mode-hook))
;;;(add-hook hook (lambda () (flyspell-mode -1))))

;;;;Set text-mode to automatically use long-lines mode.
;(add-hook 'text-mode-hook 'longlines-mode)

;;;;This apparently allows seamless editting of files in a tar/jar/zip
;;;;file.
(auto-compression-mode 1)

;;;;Highlight regions so one can see what one is doing...
;(transient-mark-mode )

;;;;Set so when moving by page, last visible line is highlighted.
(load "highlight-context-line.el")

;;;;I like M-g for goto-line
(global-set-key "\M-g" 'goto-line)

;;;;Change C-x C-b behavior so it uses bs; shows only interesting
;;;;buffers.
(global-set-key "\C-x\C-b" 'bs-show)

;;;;M-dn and M-up do nothing! :(  Let's make them do something, like M-
;;;left and M-right do.
;;;(global-set-key [M-down] 'beginning-of-buffer)
;;;(global-set-key [M-up]   'end-of-buffer)

;;;;Set up msb mode. It is not as quick to use as regular buffer tab,
;;;;but for those I use C-x b, not the menu.
(require 'msb)
(msb-mode 1) 

(defun shell-dwim (&optional create)
   "Start or switch to an inferior shell process, in a smart way.
 If a buffer with a running shell process exists, simply switch to
 that buffer.
 If a shell buffer exists, but the shell process is not running,
 restart the shell.
 If already in an active shell buffer, switch to the next one, if
 any.

 With prefix argument CREATE always start a new shell."
   (interactive "P")
   (let* ((next-shell-buffer
           (catch 'found
             (dolist (buffer (reverse (buffer-list)))
               (when (string-match "^\\*shell\\*" (buffer-name buffer))
                 (throw 'found buffer)))))
          (buffer (if create
                      (generate-new-buffer-name "*shell*")
                    next-shell-buffer)))
     (shell buffer)))

;;;; (global-set-key [f7] 'alt-shell-dwim)

 (defun alt-shell-dwim (arg)
   "Run an inferior shell like `shell'. If an inferior shell as its I/O
 through the current buffer, then pop the next buffer in `buffer-list'
 whose name is generated from the string \"*shell*\". When called with
 an argument, start a new inferior shell whose I/O will go to a buffer
 named after the string \"*shell*\" using `generate-new-buffer-name'."
   (interactive "P")
   (let* ((shell-buffer-list
 	  (let (blist)
	     (dolist (buff (buffer-list) blist)
	       (when (string-match "^\\*shell\\*" (buffer-name buff))
	 	(setq blist (cons buff blist))))))
	  (name (if arg
	 	   (generate-new-buffer-name "*shell*")
	 	 (car shell-buffer-list))))
     (shell name)))

;;;;Enable iswitchb buffer mode. I find it easier to use than the 
;;;;regular buffer switching. While we are messing with buffer
;;;;movement, the second sexp hides all the buffers beginning 
;;;;with "*". The third and fourth sexp does some remapping. 
;;;;My instinct is to go left-right in a completion buffer, not C-s/C-r
;;;;(iswitchb-mode 1)
(defun iswitchb-local-keys ()
  (mapc (lambda (K) 
	  (let* ((key (car K)) (fun (cdr K)))
            (defvar iswitchb-mode-map)
	    (define-key iswitchb-mode-map (edmacro-parse-keys key)
fun)))
	'(("<right>" . iswitchb-next-match)
	  ("<left>"  . iswitchb-prev-match)
	  ("<up>"    . ignore             )
	  ("<down>"  . ignore             ))))
(add-hook 'iswitchb-define-mode-map-hook 'iswitchb-local-keys)

;;;;Completion ignores filenames ending in any string in this list.
(setq completion-ignored-extensions
      '(".o" ".elc" ".class" "java~" ".ps" ".abs" ".mx" ".~jv" ))

;;;;We can also get completion in the mini-buffer as well.
(icomplete-mode t)

;;;;;Save new words in pdict without questioning
;;;(setq ispell-silently-savep t)

;;;Text files supposedly end in new lines. Or they should.
(setq require-final-newline t)

;;;;"I always compile my .emacs, saves me about two seconds
;;;;startuptime. But that only helps if the .emacs.elc is newer
;;;;than the .emacs. So compile .emacs if it's not."
(defun autocompile nil
  "compile itself if ~/.emacs"
  (interactive)
  (require 'bytecomp)
  (if (string= (buffer-file-name) (expand-file-name (concat
default-directory ".emacs")))
      (byte-compile-file (buffer-file-name))))

(add-hook 'after-save-hook 'autocompile)

;;;;"Redefine the Home/End keys to (nearly) the same as visual studio
;;;;behavior... special home and end by Shan-leung Maverick WOO
;;;;<sw77@cornell.edu>"
;;;;This is complex. In short, the first invocation of Home/End moves
;;;;to the beginning of the *text* line. A second invocation moves the
;;;;cursor to the beginning of the *absolute* line. Most of the time 
;;;;this won't matter or even be noticeable, but when it does (in 
;;;;comments, for example) it will be quite convenient.
(global-set-key [home] 'My-smart-home)
(global-set-key [end] 'My-smart-end)
(defun My-smart-home ()
  "Odd home to beginning of line, even home to beginning of
text/code."
  (interactive)
  (if (and (eq last-command 'My-smart-home)
	   (/= (line-beginning-position) (point)))
      (beginning-of-line)
    (beginning-of-line-text))
  )
(defun My-smart-end ()
  "Odd end to end of line, even end to begin of text/code."
  (interactive)
  (if (and (eq last-command 'My-smart-end)
	   (= (line-end-position) (point)))
      (end-of-line-text)
    (end-of-line))
  )
(defun end-of-line-text ()
  "Move to end of current line and skip comments and trailing space.
Require `font-lock'."
  (interactive)
  (end-of-line)
  (let ((bol (line-beginning-position)))
    (unless (eq font-lock-comment-face (get-text-property bol 'face))
      (while (and (/= bol (point))
		  (eq font-lock-comment-face
		      (get-text-property (point) 'face)))
	(backward-char 1))
      (unless (= (point) bol)
	(forward-char 1) (skip-chars-backward " \t\n"))))
  ) ;;;;Done with home and end keys.

;;;;But what about the normal use for home and end? 
;;;;We can still have them! Just prefixed with control.
(global-set-key [\C-home] 'beginning-of-buffer)
(global-set-key [\C-end] 'end-of-buffer)

;;;;"These tell emacs to associate certain filename extensions with 
;;;;certain modes.  I use cc-mode.el (c++-mode) for C as well as C++
;;;;code.  It is fairly all-encompassing, also working with other
;;;;C-like languages, such as Objective C and Java."
(setq auto-mode-alist (cons '("\\.text$" . text-mode)
			    auto-mode-alist))
;;(setq auto-mode-alist (cons '("\\.txt$" . text-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.txt$" . org-mode) auto-mode-alist))
;;; .org seems automatically in org-mode.
;;; (setq auto-mode-alist (cons '("\\.org$" . org-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.doc$" . text-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.doc$" . text-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.awk$" . awk-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.perl$" . perl-mode)
			    auto-mode-alist))
(setq auto-mode-alist (cons '("\\.plx$" . perl-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.pl$" . perl-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.C$" . c++-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.cc$" . c++-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.c$" . c++-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.h$" . c++-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.cpp$" . c++-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.cxx$" . c++-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.tcl$" . tcl-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.sh$" . shell-script-mode) 
			    auto-mode-alist))
(setq auto-mode-alist (cons '("\\.zsh$" . shell-script-mode)
			    auto-mode-alist))
(set-default 'auto-mode-alist
	     (append '(("\\.lisp$" . lisp-mode)) auto-mode-alist))
(set-default 'auto-mode-alist
	     (append '(("TODO" . text-mode)) auto-mode-alist))

(set-default 'auto-mode-alist
	     (append '(("README" . text-mode)) auto-mode-alist))

;; (setq auto-mode-alist (cons '("\\.yang$" . yang-mode) auto-mode-alist))

(set-default 'auto-mode-alist
	     (append '(("dir" . info-mode)) auto-mode-alist))

(setq completion-ignored-extensions;; Filename completion ignores
;;these.
      (append completion-ignored-extensions 
	      '(".CKP" ".u" ".press" ".imp" ".BAK")))

(put 'eval-expression 'disabled nil)

;;;;Make sure that .emacs file is edited in lisp mode:
(setq auto-mode-alist (cons '("\.emacs" . lisp-mode) auto-mode-alist))

;;;;These lines tell emacs to automagically font-lock buffers which
;;;;are covered by various modes. 
(setq 
 emacs-lisp-mode-hook '(lambda () (font-lock-mode 1))
 lisp-mode-hook '(lambda () (font-lock-mode 1))
 c-mode-hook '(lambda () (font-lock-mode 1))
 c++-mode-hook '(lambda () (font-lock-mode 1))
 html-mode-hook '(lambda () (font-lock-mode 1))
 makefile-mode-hook '(lambda () (font-lock-mode 1))
 shell-mode-hook '(lambda () (font-lock-mode 1))
 dired-mode-hook '(lambda () (font-lock-mode 1))
 rmail-mode-hook '(lambda () (font-lock-mode 1))
 compilation-mode-hook '(lambda () (font-lock-mode 1)))
    
;;;;ERC customizations
(setq erc-echo-notices-in-minibuffer-flag t)

;;;;Try'n'get tramp working. Commented out because this is the slowest
;;;;part of this whole .emacs according to pod.el, and I don't use it
;;;;regularly.
(require 'tramp)
(setq tramp-default-method "ssh")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;MODES;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;I send all customizations dealing with lisp files which set new
;;;modes here. Believe it or not, all the preceding dealt with vanilla 
;;;emacs options.

;;;;Set up ibs- lets you cycle through buffers using C-tab
;;;;http://www.geekware.de/software/emacs/#ibs
(require 'ibs)

;;;;Add browse-kill-ring extension. Lets you see your kill-ring as a
;;;;buffer.
;;;;http://www.todesschaf.org/projects/bkr.html
;(autoload 'browse-kill-ring "~/.emacs.d/browse-kill-ring.el" "Enables
;opening up of kill-ring in a buffer" t)

;;;;Make sure eterm is a possibility. Opens up a terminal in a new
;;;;frame. Better than M-x shell, usually.
;(require 'eterm)

;;;;Turn on a sort of flashing parentheses mode borrowed from Zmacs.
;;;;http://www.splode.com/~friedman/software/emacs-lisp/src/flash-paren.el
(autoload 'flash-paren-mode "~/.emacs.d/lisp/flash-paren.el" 
  "Enable Zmacs style flashing of parentheses" t)
(flash-paren-mode 1)

;;;;Turn on a timeout for the parentheses; normally they blink
;;;;indefinitely, but this packages changes the length of time they 
;;;;will blink to ~.6 seconds.
;;;;The timeout can be changed. I've set it to 1.5 seconds.
;;;;http://web.comhem.se/~u83406637/emacs/paren-glint.el
;;;;(autoload 'paren-glint "~/.emacs.d/lisp/paren-glint.el" 
;;;;  "Adds a timeout for flashed parentheses." t)
;;;;(require 'paren-glint)
;;;;(paren-glint-mode 1)

;;;More display goodness- highlight-tail mode, which
;;;;colorizes recently added text, but the color diminishes
;;;;over time, so you can see what you most recently typed.
;;;;http://groups.google.com/group/gnu.emacs.sources/msg/b50d53424e7ca0cd?output=gplain
;(require 'highlight-tail)
;(highlight-tail-reload)

;;;;This supposedly lets the VC interface use darcs.
;;;;http://www.emacswiki.org/cgi-bin/wiki/vc-darcs.el
;(require 'vc-darcs)
;(add-to-list 'vc-handled-backends 'DARCS)

;;;;Auto-capitalize mode
;;;;http://www.emacswiki.org/cgi-bin/wiki/auto-capitalize.el
;(autoload 'auto-capitalize-mode "auto-capitalize"
;  "Toggle `auto-capitalize' minor mode in this buffer." t)
;(autoload 'turn-on-auto-capitalize-mode "auto-capitalize"
;  "Turn on `auto-capitalize' minor mode in this buffer." t)
;(autoload 'enable-auto-capitalize-mode "auto-capitalize"
;  "Enable `auto-capitalize' minor mode in this buffer." t)

;;;;Sessions mode
;;;;http://emacs-session.sourceforge.net/
(require 'session)
(add-hook 'after-init-hook 'session-initialize)
(autoload 'session "~/.emacs.d/lisp/session.el" "This saves 
certain variables like input histories." t)

;;;;"Recentf is a minor mode that builds a list of recently opened
;;;;files. This list is is automatically saved across Emacs sessions. 
;;;;You can then access this list through a menu."
;;;;http://www.emacswiki.org/cgi-bin/wiki/recentf-buffer.el
(require 'recentf)
(setq recentf-auto-cleanup 'never) ;;To protect tramp
(recentf-mode 1)

;;;;Enable color-theme, a large collection of graphical themes and 
;;;;commands for managing them. Also, choose a random one on boot.
;;;;http://www.emacswiki.org/cgi-bin/wiki/ColorTheme
;(autoload 'color-theme "~/.emacs.d/color-theme.el" "Mode
;which changes the graphical theme." t)

;;;;Enable setnu, set it to the setnu+ expansion, and turn it on for
;;;;text.
;;;;http://www.emacswiki.org/cgi-bin/wiki/setnu%2b.el
;autoload 'setnu-mode "~/.emacs.d/setnu+.el" "Minor mode for
;numbering lines for text." t)
;(add-hook 'text-mode-hook 'setnu-mode)

;;;;Force dupwords to be loaded. Dupwords goes through a text looking
;;;;for areas with the same word more than once within a set range.
;;;;Remember! The commands don't start with "dupwords", but with "dw"!
;;;;http://www.damtp.cam.ac.uk/user/sje30/emacs/dupwords.el
; autoload 'dupwords "~/.emacs.d/dupwords.el" "A command which
;finds duplicate words in a buffer." t)

;;;;Wikipedia mode- syntax highlighting for Wikipedia, plus some 
;;;;binding of moving around commands.
;;;;http://www.emacswiki.org/cgi-bin/wiki/WikipediaMode
;(autoload 'wikipedia-mode "~/.emacs.d/wikipedia-mode.el"
;  "Major mode for editing documents in Wikipedia markup." t)
;(add-to-list 'auto-mode-alist 
;	     '("\\.wiki\\'" . wikipedia-mode)) ;;;;Duh.
;(add-to-list 'auto-mode-alist 
;	     '("index.\\.*" . wikipedia-mode))
;;;;add auto-capitalize in addition to wikipedia-mode.
;(add-to-list 'auto-mode-alist 
;	     '("index.\\.*" . auto-capitalize)) 
;;;;Add check for duplicate words to wikipedia-mode.
;(add-to-list 'auto-mode-alist 
;	     '("index.\\.*" . dw-check-to-end)) 
;;;;Pabbrev mode too. Very useful.
;(add-to-list 'auto-mode-alist 
;	     '("index.\\.*" . 'pabbrev-scavenge-buffer)) 
;(add-to-list 'auto-mode-alist 
;	     '("index.\\.*" .  'pabbrev-mode)) 
;;;;Flyspell mode is good as well.
;(add-to-list 'auto-mode-alist 
;	     '("index.\\.*" . 'flyspell-mode)) 
;(add-to-list 'auto-mode-alist 
;	     '("index.\\.*" . 'flyspell-buffer)) 

;;;;Darcs mode. not yet sure how to use it.
;;;;http://www.emacswiki.org/cgi-bin/wiki/vc-darcs.el
;(autoload 'darcs-mode "~/.emacs.d/darcsum.el" "Minor mode
;for dealing with the darcs repository." t)

;;;;Crontab mode
;;;;http://www.mahalito.net/~harley/elisp/crontab-mode.el
;(autoload 'crontab-mode "~/.emacs.d/crontab-mode.el" "Major mode
;for editing the crontab" t)
;(add-to-list 'auto-mode-alist '("\\.cron\\(tab\\)?\\'" .
;crontab-mode))

;;;;.ratpoisonrc mode
;(autoload 'ratpoisonrc-mode "~/.emacs.d/ratpoison.el"
;  "Major mode for editting ratpoison rc file." t)
;(add-to-list 'auto-mode-alist '("\\.ratpoison\\(rc\\)?\\'" .
;ratpoisonrc-mode))

;; (autoload 'yang-mode "yang-mode.el"
;; "Major mode for editing YANG documents." t)

(autoload 'yang-mode "yang-mode" "Major mode for editing YANG modules." t)
(add-to-list 'auto-mode-alist '("\\.yang$" . yang-mode))
(setq blink-matching-paren-distance nil)

;;;;Remember.el is gotten from the Debian package.
;;;;This tells remember to append notes to ~/.notes
;;;;http://packages.debian.org/stable/misc/remember-el
;(setq remember-handler-functions 
;      '(remember-append-to-file))

;;;;Dired-sort-menu adds sorting of a dired-shown directory.
;;;;http://centaur.maths.qmw.ac.uk/Emacs/files/dired-sort-menu.el
;(autoload 'dired-sort-menu "~/.emacs.d/dired-sort-menu.el" 
;  "Minor mode adding ls sort options to dired." t)
;(add-hook 'dired-load-hook
;           (lambda () (require 'dired-sort-menu)))

;;;;Add in wide-column. Informs via changing cursor color when a line
;;;;is too wide, past 72 columns. My monitor is too wide, so making 
;;;;it really wide is an easy mistake. This also enables it for text mode.
;;;;http://homepages.cs.ncl.ac.uk/phillip.lord/download/emacs/wide-column.el
;(require 'wide-column)
;(add-hook 'text-mode-hook 'wide-column-mode)

;;;;Enable undoc; a mode which edits MS Word .doc files.
;;;;http://www.ccs.neu.edu/home/guttman/undoc.el
(autoload 'undoc "~/.emacs.d/lisp/undoc.el" "A minor mode which kills MS
Word files dead." t)
(autoload 'undoc-current-buffer "undoc" "" t)
(autoload 'undoc-region-after-mime-decode "undoc" "" t)

;;;;;;;;;;;;;;;;;;;;;;;;;;MISC;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;this goes into .Xmodmap; should kill caps lock dead:
;;; !
;;; ! Swap Caps_Lock and Control_L
;;; !
;;; remove Lock = Caps_Lock
;;; remove Control = Control_L
;;; ! Don't swap, forget it.
;;; !keysym Control_L = Caps_Lock
;;; keysym Caps_Lock = Control_L
;;; !add Lock = Caps_Lock
;;; add Control = Control_L


;;;;;;;;;;;CUSTOM;;;;;;;;;;;;;;;;;;;;
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("4c7a1f0559674bf6d5dd06ec52c8badc5ba6e091f954ea364a020ed702665aa1" default)))
 '(enable-recursive-minibuffers t)
 '(erc-autojoin-channels-alist (quote (("irc.freenode.net" "#Emacs" "#Wikipedia"))))
 '(erc-away-nickname "MaruTheSandman")
 '(erc-fill-column 82)
 '(erc-join-buffer (quote bury))
 '(erc-manual-set-nick-on-bad-nick-p t)
 '(erc-modules
   (quote
    (autojoin button fill irccontrols match netsplit noncommands pcomplete ring scrolltobottom services sound stamp track)))
 '(erc-nick "Marudubshinki")
 '(erc-prompt-for-password nil)
 '(erc-reuse-buffers nil)
 '(erc-server "irc.freenode.net")
 '(erc-user-full-name "maru dubshinki")
 '(erc-whowas-on-nosuchnick t)
 '(icomplete-compute-delay 0.2)
 '(iswitchb-case t)
 '(iswitchb-max-to-show 10)
 '(iswitchb-mode t)
 '(iswitchb-prompt-newbuffer nil)
 '(iswitchb-regexp t)
 '(iswitchb-use-frame-buffer-list t)
 '(iswitchb-use-virtual-buffers t nil (recentf))
 '(kill-ring-max 120)
 '(kill-whole-line t)
 '(package-selected-packages
   (quote
    (helm-etags-plus markdown-mode counsel-projectile projectile zenburn-theme helm-swoop helm-descbinds helm flx counsel-tramp counsel-gtags counsel-etags counsel swiper ivy use-package ag)))
 '(type-break-good-break-interval 60)
 '(type-break-good-rest-interval 200)
 '(type-break-mode t nil (type-break)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(CUA-rectangle-noselect-face ((t (:background "light gray" :foreground "pale green")))))
;;;; from above line, I removed cnfonts, pyim
;;    (flx cnfonts pyim counsel-tramp counsel-gtags counsel-etags counsel swiper ivy use-package ag)))

(setq debug-on-error nil);was set t at top of buffer

(fset 'delete-current-line
   "\C-a\C-k")

(require 'auto-complete)
(global-auto-complete-mode t)
(define-key ac-complete-mode-map "\C-n" 'ac-next)
(define-key ac-complete-mode-map "\C-p" 'ac-previous)
;;; (setq ac-auto-start nil)
(global-set-key "\M-/" 'ac-start)
(define-key ac-complete-mode-map "\M-/" 'ac-stop)

;; Completion by TAB
;; -----------------
;;
;; Add following code to your .emacs.
;;
(define-key ac-complete-mode-map "\t" 'ac-complete)
(define-key ac-complete-mode-map "\r" nil)

(load-file "~/.emacs.d/site-lisp/id-utils.el")

(require 'color-theme)
(color-theme-initialize)
;;(color-theme-robin-hood)
(color-theme-clarity)
;;(color-theme-charcoal-black)
;;(color-theme-ld-dark)

;; (defun my-yang-mode-hook ()
;;   "Configuration for YANG Mode. Add this to `yang-mode-hook'."
;;  (if window-system
;;   (progn
;;	(c-set-style "BSD")
;;	(setq indent-tabs-mode nil)
;;	(let (c-basic-offset (2)))
;;	(setq c-basic-offset 2)
;;	(setq font-lock-maximum-decoration t)
;;	(font-lock-mode t))))
;;

(defun my-yang-mode-hook ()
  "Configuration for YANG Mode. Add this to `yang-mode-hook'."
  (if window-system
      (progn
        (c-set-style "BSD")
        (setq indent-tabs-mode nil)
        (setq c-basic-offset 4)
        (setq font-lock-maximum-decoration t)
        (font-lock-mode t))))
     
(add-hook 'yang-mode-hook 'my-yang-mode-hook)

(setq auto-mode-alist (cons '("\\.xml$" . nxml-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.xsl$" . nxml-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.xhtml$" . nxml-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.page$" . nxml-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.cli$" . nxml-mode) auto-mode-alist))

(autoload 'xml-mode "nxml" "XML editing mode" t)

;;;; commented out due to error
;;;;(eval-after-load 'rng-loc
;;;;  '(add-to-list 'rng-schema-locating-files "~/.schema/schemas.xml"))

(global-set-key [C-return] 'completion-at-point)

;;;;
;;;; below code for find-file-root
;;;; Type "C-x C-r" to sudo edit file
;;;;
(defvar find-file-root-prefix (if (featurep 'xemacs) "/[sudo/root@localhost]" "/sudo:root@localhost:" )
  "*The filename prefix used to open a file with `find-file-root'.")

(defvar find-file-root-history nil
  "History list for files found using `find-file-root'.")

(defvar find-file-root-hook nil
  "Normal hook for functions to run after finding a \"root\" file.")

(defun find-file-root ()
  "*Open a file as the root user.
   Prepends `find-file-root-prefix' to the selected file name so that it
   maybe accessed via the corresponding tramp method."

  (interactive)
  (require 'tramp)
  (let* ( ;; We bind the variable `file-name-history' locally so we can
	 ;; use a separate history list for "root" files.
	 (file-name-history find-file-root-history)
	 (name (or buffer-file-name default-directory))
	 (tramp (and (tramp-tramp-file-p name)
		     (tramp-dissect-file-name name)))
	 path dir file)

    ;; If called from a "root" file, we need to fix up the path.
    (when tramp
      (setq path (tramp-file-name-localname tramp)
	    dir (file-name-directory path)))

    (when (setq file (read-file-name "Find file (UID = 0): " dir path))
      (find-file (concat find-file-root-prefix file))
      ;; If this all succeeded save our new history list.
      (setq find-file-root-history file-name-history)
      ;; allow some user customization
      (run-hooks 'find-file-root-hook))))

(global-set-key [(control x) (control r)] 'find-file-root)
;;;;
;;;; end of find-file-root
;;;;

;;;;
;;;; Edit the same file as sudo.
;;;;
;;;; allow you to reopen an existing file with sudo, without needing to
;;;; navigate to it or loose your place in the file.
;;; Also works on local or remote files.
;;;;
(set-default 'tramp-default-proxies-alist (quote ((".*" "\\`root\\'" "/ssh:%h:"))))
(require 'tramp)
(defun sudo-edit-current-file ()
  (interactive)
  (let ((position (point)))
    (find-alternate-file
     (if (file-remote-p (buffer-file-name))
         (let ((vec (tramp-dissect-file-name (buffer-file-name))))
           (tramp-make-tramp-file-name
            "sudo"
            (tramp-file-name-user vec)
            (tramp-file-name-host vec)
            (tramp-file-name-localname vec)))
       (concat "/sudo:root@localhost:" (buffer-file-name))))
    (goto-char position)))
(global-set-key [(control c) (control r)] 'sudo-edit-current-file)
;;;;
;;;; End of Edit the same file as sudo.
;;;;
