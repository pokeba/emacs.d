;;; Saved through ges-version 0.3.3dev at 2003-03-04 13:35
;;; ;;; From: Richard Klinda <ignotus-dated-1047316113.43a5c3@my.gnus.org>
;;; ;;; Subject: cyclebuffer.el 1.4 --- select buffer by cycling through
;;; ;;; Newsgroups: gnu.emacs.sources
;;; ;;; Date: Tue, 04 Mar 2003 18:43:48 +0100
;;; ;;; Organization: I'd like MY data-base JULIENNED and stir-fried!

;;; Dear Users,

;;; Here (below my sig) is cyclebuffer 1.4 with improved documentation
;;; (thanks Johan) and a new feature, skipping visible buffers (thank you
;;; Dan).

;;; Meanwhile Ted Zlatanov kindly pointed out that a very similar package
;;; cycle-buffer.el exists (notice the dash).  Obviously they share the same
;;; roots, cycle-buffer.el is clearly more advanced (IOW makes cyclebuffer
;;; obsolete), so if you like cyclebuffer.el then make sure you check out
;;; cycle-buffer.el too!

;;; Here is a link to cycle-buffer.el I found via Google:
;;; http://ftp.codefactory.se/pub/elisp/elisp-archive/convenience/cycle-buffer.el

;;; -- 
;;; Richard
;;;                                              A coward dies a thousand deaths.

;;; ; > ####################################################### CUT HERE ###
;;; cyclebuffer.el --- select buffer by cycling through

;;; Commentary:

;; Description:
;; ------------
;; Cyclebuffer is yet another way of selecting buffers.  Instead of
;; prompting you for a buffer name, cyclebuffer-forward switches to the
;; most recently used buffer, and repeated invocations of
;; cyclebuffer-forward switch to less recently visited buffers.  If you
;; accidentally overshoot, calling cyclebuffer-backward goes back.
;;
;; I find this to be the fastest buffer-switching mechanism; it`s like C-x
;; b <return> w/out the return, but it`s not limited to the most recently
;; accessed buffer.  Plus you never have to remember buffer names; you
;; just keep cycling until you recognize the buffer you`re searching for.

;; Note:
;; -----
;; There is a very similar package named cycle-buffer.el that has more
;; features than cyclebuffer.el.  You can download cycle-buffer.el from
;; http://ftp.codefactory.se/pub/elisp/elisp-archive/convenience/cycle-buffer.el

;; Installation:
;; -------------
;;   Add these lines in your .emacs:
;;     (autoload `cyclebuffer-forward "cyclebuffer" "cycle forward" t)
;;     (autoload `cyclebuffer-backward "cyclebuffer" "cycle backward" t)
;;     (global-set-key "\M-N" 'cyclebuffer-forward)
;;     (global-set-key "\M-P" 'cyclebuffer-backward)
;;
;;   If you think you may need cyclebuffer-forward-same-mode and
;;   cyclebuffer-backward-same-mode, don't forget to bind them too:
;;     (global-set-key "\C-\M-N" 'cyclebuffer-forward-same-mode)
;;     (global-set-key "\C-\M-P" 'cyclebuffer-backward-same-mode)
;;
;;   You may want to adjust the keyboard bindings to avoid conflicts
;;   with whatever other packages you`re using...
;;
;;   {rklinda} I like to bind cyclebuffer functions to the F keys F11,
;;   F12 and C-F11, C-F12.

;; Change History:
;; ===============
;;  * 18 Feb 98 v1.2
;;     - Bug fixes, code simplification.
;;  * 01 Mar 03 v1.3
;;     - New functions (*-same-mode) and new maintainer.
;;  * 04 Mar 03 v1.4
;;     - Skipping buffers visible in current frame (patch by Dan Girellini)
;;
;; Thanks to Henry Harpending for suggestions.
;;
;; Author: Kurt Partridge <kepart@cs.washington.edu>
;; Maintainer: Richard Klinda <ignotus@my.gnus.org> (TMDA address)
;; Created: 05 June 1996
;; Version: $Revision: 1.4 $
;; URL: http://ignotus.linuxforum.hu/cyclebuffer.el
;; Keywords: switch-to-buffer

;; LCD Archive Entry:
;; cyclebuffer|Richard Klinda|ignotus@my.gnus.org|
;; Select buffers by cycling.|
;; 04-Mar-03|Version 1.4|

;; Copyright (C) 1996 Kurt Partridge

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, 675 Massachusettes Ave,
;; Cambridge, MA 02139, USA.

(defvar cyclebuffer-buffer-list nil
  "List of all buffers; updated every time a new set of cyclebuffer
commands are started.")

(defvar cyclebuffer-buffer-index nil
  "Number indicating the index of the buffer in
cyclebuffer-buffer-list that is currently being displayed.")

(defun cyclebuffer-forward (&optional direction same-major-p)
  "Like switch-to-buffer, but doesn`t prompt.  Repetitive invocations of
this function select progressively less recently visited buffers.  If
same-major-p is T then it will only select buffers that has the same
major mode as the starting one."
  (interactive "P")
  ;; If starting a new search, a) make sure the current buffer is at top
  ;; of the list of buffers, and b) set flag to generate a new list
  (when (not (or (eq `cyclebuffer-forward last-command)
                 (eq `cyclebuffer-backward last-command)
                 (eq `cyclebuffer-forward-same-mode last-command)
                 (eq `cyclebuffer-backward-same-mode last-command)))
    (setq cyclebuffer-buffer-index nil)
    (switch-to-buffer (current-buffer)))
  ;; Generate new list if necessary
  (when (not (numberp cyclebuffer-buffer-index))
    (let ((major major-mode))
      (setq cyclebuffer-buffer-list (if same-major-p
                                        (loop for buffer in (buffer-list)
                                              when (and (set-buffer buffer)
                                                        (eq major-mode major))
                                              collect buffer)
                                        (buffer-list)))
      (setq cyclebuffer-buffer-index 0)))
  ;; Cycle through buffers, skipping any invisible buffers (whose
  ;; names start with a blank space)
  (let ((start-buffer (current-buffer))
	(chosen-buffer (current-buffer)))
    (while (or (eq chosen-buffer start-buffer)
               (eq (first (frames-of-buffer chosen-buffer)) (selected-frame))
	       (char-equal ?  (string-to-char (buffer-name chosen-buffer))))
      (setq start-buffer nil)
      (if (or (null direction) (eq direction 1))
	  (setq cyclebuffer-buffer-index (+ cyclebuffer-buffer-index 1))
	(setq cyclebuffer-buffer-index (- cyclebuffer-buffer-index 1)))
      (setq cyclebuffer-buffer-index
	    (mod cyclebuffer-buffer-index (length cyclebuffer-buffer-list)))
      (setq chosen-buffer (nth cyclebuffer-buffer-index
cyclebuffer-buffer-list)))
    (switch-to-buffer chosen-buffer)))

(defun cyclebuffer-backward (&optional same-major-p)
  "Like cyclebuffer-forward, but selects progressively more recently
visited buffers."
  (interactive)
  (cyclebuffer-forward -1 same-major-p))

(defun cyclebuffer-forward-same-mode (&optional direction)
  "This is a variation of cyclebuffer-forward, it selects buffers with
the same major-mode as the starting one."
  (interactive)
  (cyclebuffer-forward direction t))

(defun cyclebuffer-backward-same-mode ()
  "Like cyclebuffer-forward-same-mode, but selects progressively more
recently visited buffers with the same major-mode as the starting one."
  (interactive)
  (cyclebuffer-forward-same-mode -1))

(provide 'cyclebuffer)


