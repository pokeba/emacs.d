<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html><head><title>EmacsWiki: auto-capitalize.el</title>
<link type="text/css" rel="stylesheet" href="auto-capitalize_files/wiki.css"><meta name="robots" content="INDEX,NOFOLLOW"><link rel="alternate" type="application/rss+xml" title="Emacs Wiki with page content" href="http://www.emacswiki.org/emacs/full.rss"><link rel="alternate" type="application/rss+xml" title="Emacs Wiki with page content and diff" href="http://www.emacswiki.org/emacs/full-diff.rss"><link rel="alternate" type="application/rss+xml" title="Emacs Wiki including minor differences" href="http://www.emacswiki.org/emacs/minor-edits.rss"></head><body class="http://www.emacswiki.org/cgi-bin/wiki"><div class="header"><a class="logo" href="http://www.emacswiki.org/cgi-bin/wiki/SiteMap"><img class="logo" src="auto-capitalize_files/emacs_logo.png" alt="[Home]"></a><span class="gotobar bar"><a class="local" href="http://www.emacswiki.org/cgi-bin/wiki/SiteMap">SiteMap</a> <a class="local" href="http://www.emacswiki.org/cgi-bin/wiki/Search">Search</a> <a class="local" href="http://www.emacswiki.org/cgi-bin/wiki/ElispArea">ElispArea</a> <a class="local" href="http://www.emacswiki.org/cgi-bin/wiki/HowTo">HowTo</a> <a class="local" href="http://www.emacswiki.org/cgi-bin/wiki/RecentChanges">RecentChanges</a> <a class="local" href="http://www.emacswiki.org/cgi-bin/wiki/News">News</a> <a class="local" href="http://www.emacswiki.org/cgi-bin/wiki/Problems">Problems</a> <a class="local" href="http://www.emacswiki.org/cgi-bin/wiki/Suggestions">Suggestions</a> </span><form class="tiny" action="http://www.emacswiki.org/cgi-bin/wiki"><p>Search: <input name="search" size="20" type="text"></p></form><br><span class="specialdays">Bolivia, Independence Day</span><h1><a title="Click to search for references to this page" href="http://www.emacswiki.org/cgi-bin/wiki?search=%22auto-capitalize%5c.el%22">auto-capitalize.el</a></h1></div><div class="wrapper"><div class="content browse"><p></p><p><a href="http://www.emacswiki.org/cgi-bin/emacs-de/download/auto-capitalize.el">Download</a></p><pre class="source"><pre class="code"><span class="linecomment">;;; auto-capitalize.el --- Automatically capitalize (or upcase) words</span>
<span class="linecomment">;;; -*-unibyte: t; coding: iso-8859-1;-*-</span>

<span class="linecomment">;; Copyright � 1998,2001,2002,2005 Kevin Rodgers</span>

<span class="linecomment">;; Author: Kevin Rodgers &lt;ihs_4664@yahoo.com&gt;</span>
<span class="linecomment">;; Created: 20 May 1998</span>
<span class="linecomment">;; Version: $Revision: 2.20 $</span>
<span class="linecomment">;; Keywords: text, wp, convenience</span>
<span class="linecomment">;; RCS $Id: auto-capitalize.el,v 2.20 2005/05/25 18:47:22 kevinr Exp $</span>

<span class="linecomment">;; This program is free software; you can redistribute it and/or</span>
<span class="linecomment">;; modify it under the terms of the GNU General Public License as</span>
<span class="linecomment">;; published by the Free Software Foundation; either version 2 of</span>
<span class="linecomment">;; the License, or (at your option) any later version.</span>

<span class="linecomment">;; This program is distributed in the hope that it will be</span>
<span class="linecomment">;; useful, but WITHOUT ANY WARRANTY; without even the implied</span>
<span class="linecomment">;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR</span>
<span class="linecomment">;; PURPOSE.  See the GNU General Public License for more details.</span>

<span class="linecomment">;; You should have received a copy of the GNU General Public</span>
<span class="linecomment">;; License along with this program; if not, write to the Free</span>
<span class="linecomment">;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,</span>
<span class="linecomment">;; MA 02111-1307 USA</span>

<span class="linecomment">;;; Commentary:</span>

<span class="linecomment">;; In `auto-capitalize' minor mode, the first word at the beginning of</span>
<span class="linecomment">;; a paragraph or sentence (i.e. at `left-margin' on a line following</span>
<span class="linecomment">;; `paragraph-separate', after `paragraph-start' at `left-margin', or</span>
<span class="linecomment">;; after `sentence-end') is automatically capitalized when a following</span>
<span class="linecomment">;; whitespace or punctuation character is inserted.</span>
<span class="linecomment">;; </span>
<span class="linecomment">;; The `auto-capitalize-words' variable can be customized so that</span>
<span class="linecomment">;; commonly used proper nouns and acronyms are capitalized or upcased,</span>
<span class="linecomment">;; respectively.</span>
<span class="linecomment">;; </span>
<span class="linecomment">;; The `auto-capitalize-yank' option controls whether words in yanked</span>
<span class="linecomment">;; text should by capitalized in the same way.</span>
<span class="linecomment">;; </span>
<span class="linecomment">;; To install auto-capitalize.el, copy it to a `load-path' directory,</span>
<span class="linecomment">;; `M-x byte-compile-file' it, and add this to your</span>
<span class="linecomment">;; site-lisp/default.el or ~/.emacs file:</span>
<span class="linecomment">;; 	(autoload 'auto-capitalize-mode "auto-capitalize"</span>
<span class="linecomment">;; 	  "Toggle `auto-capitalize' minor mode in this buffer." t)</span>
<span class="linecomment">;; 	(autoload 'turn-on-auto-capitalize-mode "auto-capitalize"</span>
<span class="linecomment">;; 	  "Turn on `auto-capitalize' minor mode in this buffer." t)</span>
<span class="linecomment">;; 	(autoload 'enable-auto-capitalize-mode "auto-capitalize"</span>
<span class="linecomment">;; 	  "Enable `auto-capitalize' minor mode in this buffer." t)</span>
<span class="linecomment">;; </span>
<span class="linecomment">;; To turn on (unconditional) capitalization in all Text modes, add</span>
<span class="linecomment">;; this to your site-lisp/default.el or ~/.emacs file:</span>
<span class="linecomment">;; 	(add-hook 'text-mode-hook 'turn-on-auto-capitalize-mode)</span>
<span class="linecomment">;; To enable (interactive) capitalization in all Text modes, add this</span>
<span class="linecomment">;; to your site-lisp/default.el or ~/.emacs file:</span>
<span class="linecomment">;; 	(add-hook 'text-mode-hook 'enable-auto-capitalize-mode)</span>
<span class="linecomment">;; </span>
<span class="linecomment">;; To prevent a word from ever being capitalized or upcased</span>
<span class="linecomment">;; (e.g. "http"), simply add it (in lowercase) to the</span>
<span class="linecomment">;; `auto-capitalize-words' list.</span>
<span class="linecomment">;; </span>
<span class="linecomment">;; To prevent a word in the `auto-capitalize-words' list from being</span>
<span class="linecomment">;; capitalized or upcased in a particular context (e.g.</span>
<span class="linecomment">;; "GNU.emacs.sources"), insert the following whitespace or</span>
<span class="linecomment">;; punctuation character with `M-x quoted-insert' (e.g. `gnu C-q .').</span>
<span class="linecomment">;; </span>
<span class="linecomment">;; To enable contractions based on a word in the</span>
<span class="linecomment">;; `auto-capitalize-words' list to be capitalized or upcased</span>
<span class="linecomment">;; (e.g. "I'm") in the middle of a sentence in Text mode, define the</span>
<span class="linecomment">;; apostrophe as a punctuation character or as a symbol that joins two</span>
<span class="linecomment">;; words:</span>
<span class="linecomment">;; 	;; Use "_" instead of "." to define apostrophe as a symbol:</span>
<span class="linecomment">;; 	(modify-syntax-entry ?' ".   " text-mode-syntax-table) ; was "w   "</span>

<span class="linecomment">;;; Code:</span>

<span class="linecomment">;; Rationale:</span>
<span class="linecomment">;; </span>
<span class="linecomment">;; The implementation of auto-capitalize via an after-change-function is</span>
<span class="linecomment">;; somewhat complicated, but two simpler designs don't work due to</span>
<span class="linecomment">;; quirks in Emacs' implementation itself:</span>
<span class="linecomment">;; </span>
<span class="linecomment">;; One idea is to advise `self-insert-command' to `upcase'</span>
<span class="linecomment">;; `last-command-char' before it is run, but command_loop_1 optimizes</span>
<span class="linecomment">;; out the call to the Lisp binding with its C binding</span>
<span class="linecomment">;; (Fself_insert_command), which prevents any advice from being run.</span>
<span class="linecomment">;; </span>
<span class="linecomment">;; Another idea is to use a before-change-function to `upcase'</span>
<span class="linecomment">;; `last-command-char', but the change functions are called by</span>
<span class="linecomment">;; internal_self_insert, which has already had `last-command-char'</span>
<span class="linecomment">;; passed to it as a C function parameter by command_loop_1.</span>

<span class="linecomment">;; Package interface:</span>

(provide 'auto-capitalize)

(require 'cl)				<span class="linecomment">; find, minusp</span>


<span class="linecomment">;; User options:</span>

(defvar auto-capitalize nil
  "<span class="quote">If non-nil, the first word of a sentence is automatically capitalized.
If non-nil but not t, query the user before capitalizing a word.
This variable automatically becomes buffer-local when set in any fashion\;
see `\\[auto-capitalize-mode]', `\\[turn-on-capitalize-mode]', or
`\\[enable-auto-capitalize-mode]'.</span>")

(make-variable-buffer-local 'auto-capitalize)

(put 'auto-capitalize 'variable-interactive
     "<span class="quote">XAutomatically capitalize the first word of each sentence? (t, nil, or 'query): </span>")

(or (assq 'auto-capitalize minor-mode-alist)
    (setq minor-mode-alist
	  (cons (list 'auto-capitalize "<span class="quote"> Cap</span>") minor-mode-alist)))


(defvar auto-capitalize-yank nil
  "<span class="quote">*If non-nil, the first word of yanked sentences are automatically capitalized.</span>")

(put 'auto-capitalize-yank 'variable-interactive
     "<span class="quote">XAutomatically capitalize the first word of yanked sentences? (t or nil): </span>")


<span class="linecomment">;; User variables:</span>

(defvar auto-capitalize-words '("<span class="quote">I</span>")	<span class="linecomment">;  "Stallman" "GNU" "http"</span>
  "<span class="quote">If non-nil, a list of proper nouns or acronyms.
If `auto-capitalize' mode is on, these words will be automatically
capitalized or upcased as listed (mixed case is allowable as well), even
in the middle of a sentence.  A lowercase word will not have its case
modified.</span>")

(defvar auto-capitalize-predicate nil
  "<span class="quote">If non-nil, a function that determines whether to enable capitalization.
In auto-capitalize mode, it is called with no arguments and should return a
non-nil value if the current word is within \</span>"normal\"<span class="quote"> text.</span>")

<span class="linecomment">;; Internal variables:</span>

(defconst auto-capitalize-version "<span class="quote">$Revision: 2.20 $</span>"
  "<span class="quote">This version of auto-capitalize.el</span>")


<span class="linecomment">;; Commands:</span>
	
(defun auto-capitalize-mode (&amp;optional arg)
  "<span class="quote">Toggle `auto-capitalize' minor mode in this buffer.
With optional prefix ARG, turn `auto-capitalize' mode on iff ARG is positive.
This sets `auto-capitalize' to t or nil (for this buffer) and ensures that
`auto-capitalize' is installed in `after-change-functions' (for all buffers).</span>"
  (interactive "<span class="quote">P</span>")
  (setq auto-capitalize
	(if (null arg)
	    (not auto-capitalize)
	  (&gt; (prefix-numeric-value arg) 0)))
  (add-hook 'after-change-functions 'auto-capitalize nil t))

(defun turn-on-auto-capitalize-mode ()
  "<span class="quote">Turn on `auto-capitalize' mode in this buffer.
This sets `auto-capitalize' to t.</span>"
  (interactive)
  (auto-capitalize-mode 1))

(defun turn-off-auto-capitalize-mode ()
  "<span class="quote">Turn off `auto-capitalize' mode in this buffer.
This sets `auto-capitalize' to nil.</span>"
  (interactive)
  (auto-capitalize-mode -1))

(defun enable-auto-capitalize-mode ()
  "<span class="quote">Enable `auto-capitalize' mode in this buffer.
This sets `auto-capitalize' to `query'.</span>"
  (interactive)
  (setq auto-capitalize 'query))


<span class="linecomment">;; Internal functions:</span>

(defun auto-capitalize (beg end length)
  "<span class="quote">If `auto-capitalize' mode is on, then capitalize the previous word.
The previous word is capitalized (or upcased) if it is a member of the
`auto-capitalize-words' list; or if it begins a paragraph or sentence.

Capitalization occurs only if the current command was invoked via a
self-inserting non-word character (e.g. whitespace or punctuation)\; but
if the `auto-capitalize-yank' option is set, then the first word of
yanked sentences will be capitalized as well.

Capitalization can be disabled in specific contexts via the
`auto-capitalize-predicate' variable.

This should be installed as an `after-change-function'.</span>"
  (if (and auto-capitalize
	   (or (null auto-capitalize-predicate)
	       (funcall auto-capitalize-predicate)))
      (cond ((or (and (or (eq this-command 'self-insert-command)
			  <span class="linecomment">;; LaTeX mode binds "." to TeX-insert-punctuation,</span>
			  <span class="linecomment">;; and "\"" to TeX-insert-quote:</span>
			  (let ((key (this-command-keys)))
			    <span class="linecomment">;; XEmacs `lookup-key' signals "unable to bind</span>
			    <span class="linecomment">;; this type of event" for commands invoked via</span>
			    <span class="linecomment">;; the mouse:</span>
			    (and (if (and (vectorp key)
                                          (&gt; (length key) 0)
					  (fboundp 'misc-user-event-p)
					  (misc-user-event-p (aref key 0)))
				     nil
				   (eq (lookup-key global-map key t)
				       'self-insert-command))
				 <span class="linecomment">;; single character insertion?</span>
				 (= length 0)
				 (= (- end beg) 1))))
		      (let ((self-insert-char
			     (cond ((fboundp 'event-to-character) <span class="linecomment">; XEmacs</span>
				    (event-to-character last-command-event
							nil nil t))
				   (t last-command-event)))) <span class="linecomment">; GNU Emacs</span>
			(not (equal (char-syntax self-insert-char) ?w))))
		 (eq this-command 'newline)
		 (eq this-command 'newline-and-indent))
	     <span class="linecomment">;; self-inserting, non-word character</span>
	     (if (and (&gt; beg (point-min))
		      (equal (char-syntax (char-after (1- beg))) ?w))
		 <span class="linecomment">;; preceded by a word character</span>
		 (save-excursion
		   (forward-word -1)
		   (save-match-data
		     (let* ((word-start (point))
			    (text-start
			     (progn
			       (while (or (minusp (skip-chars-backward "<span class="quote">\</span>""<span class="quote">))
					  (minusp (skip-syntax-backward </span>"\"<span class="quote">(</span>")))
				 t)
			       (point)))
			    lowercase-word)
		       (cond ((and auto-capitalize-words
				   (let ((case-fold-search nil))
				     (goto-char word-start)
				     (looking-at
				      (concat "<span class="quote">\\(</span>"
					      (mapconcat 'downcase
							 auto-capitalize-words
							 "<span class="quote">\\|</span>")
					      "<span class="quote">\\)\\&gt;</span>"))))
			      <span class="linecomment">;; user-specified capitalization</span>
			      (if (not (member (setq lowercase-word
						     (buffer-substring <span class="linecomment">; -no-properties?</span>
						      (match-beginning 1)
						      (match-end 1)))
					       auto-capitalize-words))
				  <span class="linecomment">;; not preserving lower case</span>
				  (progn <span class="linecomment">; capitalize!</span>
				    (undo-boundary)
				    (replace-match (find lowercase-word
							 auto-capitalize-words
							 :key 'downcase
							 :test 'string-equal)
						   t t))))
			     ((and (or (equal text-start (point-min)) <span class="linecomment">; (bobp)</span>
				       (progn <span class="linecomment">; beginning of paragraph?</span>
					 (goto-char text-start)
					 (and (= (current-column) left-margin)
					      (zerop (forward-line -1))
					      (looking-at paragraph-separate)))
				       (progn <span class="linecomment">; beginning of paragraph?</span>
					 (goto-char text-start)
					 (and (= (current-column) left-margin)
					      (re-search-backward paragraph-start
								  nil t)
					      (= (match-end 0) text-start)
					      (= (current-column) left-margin)))
				       (progn <span class="linecomment">; beginning of sentence?</span>
					 (goto-char text-start)
					 (save-restriction
					   (narrow-to-region (point-min)
							     word-start)
					   (and (re-search-backward sentence-end
								    nil t)
						(= (match-end 0) text-start)
						<span class="linecomment">;; verify: preceded by</span>
						<span class="linecomment">;; whitespace?</span>
						(let ((previous-char
						       (char-after
							(1- text-start))))
						  <span class="linecomment">;; In some modes, newline</span>
						  <span class="linecomment">;; (^J, aka LFD) is comment-</span>
						  <span class="linecomment">;; end, not whitespace:</span>
						  (or (equal previous-char
							     ?\n)
						      (equal (char-syntax
							      previous-char)
							     ? )))
						<span class="linecomment">;; verify: not preceded by</span>
						<span class="linecomment">;; an abbreviation?</span>
						(let ((case-fold-search nil)
						      (abbrev-regexp
                                                       (if (featurep 'xemacs)
                                                           "<span class="quote">\\&lt;\\([A-Z�-��-�]?[a-z�-��-�]+\\.\\)+\\=</span>"
                                                         "<span class="quote">\\&lt;\\([[:upper:]]?[[:lower:]]+\\.\\)+\\=</span>")))
						  (goto-char
						   (1+ (match-beginning 0)))
						  (or (not
						       (re-search-backward abbrev-regexp
									   nil t))
						      (not
						       (member
							(buffer-substring <span class="linecomment">; -no-properties?</span>
							 (match-beginning 0)
							 (match-end 0))
							auto-capitalize-words))))
						))))
				   <span class="linecomment">;; inserting lowercase text?</span>
				   (let ((case-fold-search nil))
				     (goto-char word-start)
				     (looking-at (if (featurep 'xemacs)
                                                     "<span class="quote">[a-z�-��-�]+</span>"
                                                   "<span class="quote">[[:lower:]]+</span>")))
				   (or (eq auto-capitalize t)
				       (prog1 (y-or-n-p
					       (format "<span class="quote">Capitalize \</span>"%s\"<span class="quote">? </span>"
						       (buffer-substring
							(match-beginning 0)
							(match-end 0))))
					 (message "<span class="quote"></span>"))))
			      <span class="linecomment">;; capitalize!</span>
			      (undo-boundary)
			      (goto-char word-start)
			      (capitalize-word 1))))))))
	    ((and auto-capitalize-yank
		  <span class="linecomment">;; `yank' sets `this-command' to t, and the</span>
		  <span class="linecomment">;; after-change-functions are run before it has been</span>
		  <span class="linecomment">;; reset:</span>
		  (or (eq this-command 'yank)
		      (and (= length 0) <span class="linecomment">; insertion?</span>
			   (eq this-command 't))))
	     (save-excursion
	       (goto-char beg)
	       (save-match-data
		 (while (re-search-forward "<span class="quote">\\Sw</span>" end t)
		   <span class="linecomment">;; recursion!</span>
		   (let* ((this-command 'self-insert-command)
			  (non-word-char (char-after (match-beginning 0)))
			  (last-command-event
			   (cond ((fboundp 'character-to-event) <span class="linecomment">; XEmacs</span>
				  (character-to-event non-word-char))
				 (t non-word-char)))) <span class="linecomment">; GNU Emacs</span>
		     (auto-capitalize (match-beginning 0)
				      (match-end 0)
				      0)))))))))

<span class="linecomment">;;; auto-capitalize.el ends here</span></pre></pre></div><div class="wrapper close"></div></div><div class="footer"><hr><span class="gotobar bar"><a class="local" href="http://www.emacswiki.org/cgi-bin/wiki/SiteMap">SiteMap</a> <a class="local" href="http://www.emacswiki.org/cgi-bin/wiki/Search">Search</a> <a class="local" href="http://www.emacswiki.org/cgi-bin/wiki/ElispArea">ElispArea</a> <a class="local" href="http://www.emacswiki.org/cgi-bin/wiki/HowTo">HowTo</a> <a class="local" href="http://www.emacswiki.org/cgi-bin/wiki/RecentChanges">RecentChanges</a> <a class="local" href="http://www.emacswiki.org/cgi-bin/wiki/News">News</a> <a class="local" href="http://www.emacswiki.org/cgi-bin/wiki/Problems">Problems</a> <a class="local" href="http://www.emacswiki.org/cgi-bin/wiki/Suggestions">Suggestions</a> </span><span class="edit bar"><br> <a class="edit" accesskey="e" title="Click to edit this page" href="http://www.emacswiki.org/cgi-bin/wiki?action=edit;id=auto-capitalize.el">Edit this page</a> <a class="history" href="http://www.emacswiki.org/cgi-bin/wiki?action=history;id=auto-capitalize.el">View other revisions</a> <a class="admin" href="http://www.emacswiki.org/cgi-bin/wiki?action=admin;id=auto-capitalize.el">Administration</a></span><span class="time"><br> Last edited 2005-10-13 17:56 UTC by <a class="author" title="from 217-162-112-104.dclient.hispeed.ch" href="http://www.emacswiki.org/cgi-bin/wiki/AlexSchroeder">AlexSchroeder</a> <a class="diff" href="http://www.emacswiki.org/cgi-bin/wiki?action=browse;diff=2;id=auto-capitalize.el">(diff)</a></span><form method="get" action="http://www.emacswiki.org/cgi-bin/wiki" enctype="multipart/form-data" class="search">
<p><label for="search">Search:</label> <input name="search" size="20" accesskey="f" id="search" type="text"> <label for="searchlang">Language:</label> <input name="lang" size="10" id="searchlang" type="text"> <input name="dosearch" value="Go!" type="submit"></p><div></div>
</form><div style="float: right; margin-left: 1ex;">
<!-- Creative Commons License -->
<a href="http://creativecommons.org/licenses/GPL/2.0/"><img alt="CC-GNU GPL" style="border: medium none ;" src="auto-capitalize_files/cc-GPL-a.png"></a>
<!-- /Creative Commons License -->
</div>

<!--
<rdf:RDF xmlns="http://web.resource.org/cc/"
 xmlns:dc="http://purl.org/dc/elements/1.1/"
 xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<Work rdf:about="">
   <license rdf:resource="http://creativecommons.org/licenses/GPL/2.0/" />
  <dc:type rdf:resource="http://purl.org/dc/dcmitype/Software" />
</Work>

<License rdf:about="http://creativecommons.org/licenses/GPL/2.0/">
   <permits rdf:resource="http://web.resource.org/cc/Reproduction" />
   <permits rdf:resource="http://web.resource.org/cc/Distribution" />
   <requires rdf:resource="http://web.resource.org/cc/Notice" />
   <permits rdf:resource="http://web.resource.org/cc/DerivativeWorks" />
   <requires rdf:resource="http://web.resource.org/cc/ShareAlike" />
   <requires rdf:resource="http://web.resource.org/cc/SourceCode" />
</License>
</rdf:RDF>
-->

<p>
This work is licensed to you under version 2 of the
<a href="http://www.gnu.org/">GNU</a> <a href="http://www.emacswiki.org/GPL">General Public License</a>.
Alternatively, you may choose to receive this work under any other
license that grants the right to use, copy, modify, and/or distribute
the work, as long as that license imposes the restriction that
derivative works have to grant the same rights and impose the same
restriction. For example, you may choose to receive this work under
the
<a href="http://www.gnu.org/">GNU</a>
<a href="http://www.emacswiki.org/FDL">Free Documentation License</a>, the
<a href="http://creativecommons.org/">CreativeCommons</a>
<a href="http://creativecommons.org/licenses/sa/1.0/">ShareAlike</a>
License, the XEmacs manual license, or
<a href="http://www.emacswiki.org/OLD">similar licenses</a>.
</p>
</div></body></html>